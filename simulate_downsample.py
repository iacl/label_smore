'''
Given an input image, downscale it either with a blur + downsample
or just a nearest neighbor downsample depending if it's an image
or mask.

The downscale factor is given as an argment
'''
import nibabel as nib
import numpy as np
import os
import argparse

from scipy.signal.windows import gaussian
from scipy.ndimage import convolve1d, rotate, zoom
from skimage.transform import rescale
from pathlib import Path


def std_to_fwhm(sigma):
    return 2 * np.sqrt(2 * np.log(2)) * sigma


def fwhm_to_std(gamma):
    return gamma / (2 * np.sqrt(2 * np.log(2)))


def fwhm_needed(fwhm_hr, fwhm_lr):
    return np.sqrt(fwhm_lr**2 - fwhm_hr**2)


def blur_and_downscale(x, max_res, min_res, axis=0):

    blur_k = fwhm_needed(min_res, max_res)
    k = max_res / min_res

    window = gaussian(int(2*round(blur_k)+1),
                      fwhm_to_std(blur_k))
    window /= window.sum()  # remove gain
    blurred = convolve1d(x, window, mode='nearest', axis=axis)

    down_scale = np.array(
        [1/k if i == axis else 1 for i in range(len(blurred.shape))])

    x_ds = rescale(
        image=blurred,
        scale=down_scale,
        mode='edge',
        order=1,
        preserve_range=True,
        anti_aliasing=False,
    )

    return x_ds


def nearest_neighbor_downscale(img, k, axis=0):
    down_scale = np.array(
        [1/k if i == axis else 1 for i in range(len(img.shape))])

    x_ds = rescale(
        image=img,
        scale=down_scale,
        mode='edge',
        order=0,
        preserve_range=True,
        anti_aliasing=False,
    )

    return x_ds


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument("--infpath", type=str)
    parser.add_argument("--outfpath", type=str)
    parser.add_argument("--downsample_factor", type=float)
    parser.add_argument("--is_mask", type=str, default='false')
    parser.add_argument("--acq_plane", type=str, default='axial')

    args = parser.parse_args()

    IN_FPATH = Path(args.infpath)
    OUT_FPATH = Path(args.outfpath)
    k = args.downsample_factor
    IS_MASK = True if args.is_mask.lower() == "true" else False

    ACQ_PLANE = args.acq_plane.lower()
    if ACQ_PLANE == "axial":
        ds_axis = 1
        affine_axis = 1
    elif ACQ_PLANE == "coronal":
        ds_axis = 0
        affine_axis = 0
    elif ACQ_PLANE == "sagittal":
        ds_axis = 2
        affine_axis = 2

    in_obj = nib.load(IN_FPATH)
    img = in_obj.get_fdata()

    if IS_MASK:
        img_ds = nearest_neighbor_downscale(img, k, axis=ds_axis)
    else:
        img_ds = blur_and_downscale(img, k, 1, axis=ds_axis)

    new_scales = [1, 1, 1]
    new_scales[affine_axis] = img.shape[affine_axis] / \
        img_ds.shape[affine_axis]
    new_scales = tuple(new_scales)
    new_affine = np.matmul(in_obj.affine, np.diag(new_scales + (1,)))

    out_obj = nib.Nifti1Image(img_ds, new_affine, header=in_obj.header)

    nib.save(out_obj, OUT_FPATH)
