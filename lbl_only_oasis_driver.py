import os
import sys
import numpy as np
from pathlib import Path


def chunks(l, n):
    n = max(1, n)
    return [l[i:i+n] for i in range(0, len(l), n)]


gpuid = sys.argv[1]
partition = int(sys.argv[2])

IN_LBL_DIR = Path(
    "/ISFILE3/USERS/remediossw/data/OASIS3/OASIS3_4x_aniso_toads_labels")

lbl_fpaths = sorted(IN_LBL_DIR.iterdir())

# split into partitions
lbl_fpaths = chunks(lbl_fpaths, 5)[partition]

RESULTDIR = Path(
    "/ISFILE3/USERS/remediossw/data/results/label_smore/OASIS/lbl_only")
LBL_RESULT_PREFIX = "SR_lbl_"

for b in lbl_fpaths:
    fname = b.name
    print("Processing {}".format(fname))
    outname = RESULTDIR / (LBL_RESULT_PREFIX + fname)
    if outname.exists():
        continue

    call = "./lbl_only_apply_sr.sh {} OASIS {}"
    os.system(call.format(gpuid, b))
