import os
import sys
import numpy as np
from pathlib import Path


def chunks(l, n):
    n = max(1, n)
    return [l[i:i+n] for i in range(0, len(l), n)]


gpuid = sys.argv[1]
partition = int(sys.argv[2])

IN_IMG_DIR = Path("/ISFILE3/USERS/remediossw/data/IXI-T1_4x_aniso")
IN_MSK_DIR = Path("/ISFILE3/USERS/remediossw/data/IXI-T1_4x_aniso_masks_toads")

img_fpaths = sorted(IN_IMG_DIR.iterdir())
msk_fpaths = sorted(IN_MSK_DIR.iterdir())

# Turn off partitions for a quick next check. Missed some for some reason.
# split into partitions
img_fpaths = chunks(img_fpaths, 5)[partition]
msk_fpaths = chunks(msk_fpaths, 5)[partition]

RESULTDIR = Path("/ISFILE3/USERS/remediossw/data/label_smore/IXI/multiblur_focalloss")
IMG_RESULT_PREFIX = "SR_img_IXI_edsr_resblocks-32_patch-32_filters-256_"
MSK_RESULT_PREFIX = "SR_mask_IXI_edsr_resblocks-32_patch-32_filters-256_"

for a, b in zip(img_fpaths, msk_fpaths):
    fname = a.name
    print("Processing {}".format(fname))
    outname = RESULTDIR / (IMG_RESULT_PREFIX + fname)
    if outname.exists():
        continue

    call = "./apply_ssr.sh {} IXI {} {}"
    os.system(call.format(gpuid, a, b))

    # Only do one right now
    # break
