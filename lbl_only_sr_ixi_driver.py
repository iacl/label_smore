import os
import sys
import numpy as np
from pathlib import Path


def chunks(l, n):
    n = max(1, n)
    return [l[i:i+n] for i in range(0, len(l), n)]


gpuid = sys.argv[1]
partition = int(sys.argv[2])

IN_MSK_DIR = Path("/ISFILE3/USERS/remediossw/data/IXI-T1_4x_aniso_masks_toads")

msk_fpaths = sorted(IN_MSK_DIR.iterdir())

# Turn off partitions for a quick next check. Missed some for some reason.
# split into partitions
msk_fpaths = chunks(msk_fpaths, 5)[partition]

RESULTDIR = Path("/ISFILE3/USERS/remediossw/data/label_smore/lbl_only")
MSK_RESULT_PREFIX = "SR_mask_IXI_edsr_resblocks-32_patch-32_filters-256_"

for b in msk_fpaths:
    fname = b.name
    print("Processing {}".format(fname))
    outname = RESULTDIR / (MSK_RESULT_PREFIX + fname)
    if outname.exists():
        continue

    call = "./lbl_only_apply_sr.sh {} IXI {}"
    os.system(call.format(gpuid, b))

    # Only do one right now
    # break
