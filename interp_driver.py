import os
import sys
import nibabel as nib
from pathlib import Path
from tqdm import tqdm

IN_IMG_DIR = Path(sys.argv[1])
IN_LBL_DIR = Path(sys.argv[2])
OUT_ROOT = Path(sys.argv[3])

LR_AXIS = 2
k = 4

img_fpaths = sorted(IN_IMG_DIR.iterdir())
lbl_fpaths = sorted(IN_LBL_DIR.iterdir())

cmd = "python src/apply_interpolate.py --infile {} --is_mask {} --outdir {} --k {} --order {} --lr_axis {}"

for img_fpath, lbl_fpath in tqdm(zip(img_fpaths, lbl_fpaths), total=len(img_fpaths)):
    # NN image
    os.system(cmd.format(img_fpath, "false", OUT_ROOT/"nn_img", k, 0, LR_AXIS))
    # bicubic image
    os.system(cmd.format(img_fpath, "false",
                         OUT_ROOT/"bicubic_img", k, 3, LR_AXIS))
    # NN label
    os.system(cmd.format(lbl_fpath, "true", OUT_ROOT/"nn_lbl", k, 0, LR_AXIS))
    # bicubic label
    os.system(cmd.format(lbl_fpath, "true",
                         OUT_ROOT/"bicubic_lbl", k, 3, LR_AXIS))
