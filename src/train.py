import argparse
import nibabel as nib
import numpy as np
import torch
import time
import sys
import os

from models.edsr import *
from models.losses import *

from utils.img_and_lbl_loader import *

from pathlib import Path
from tqdm import tqdm

np.random.seed(0)

# Optimize torch
torch.backends.cudnn.benchmark = True

if __name__ == '__main__':
    print("{a} BEGIN TRAINING {a}".format(a="="*20))

    #################### ARGUMENTS ####################
    parser = argparse.ArgumentParser()
    parser.add_argument("--infile", type=str)
    parser.add_argument("--inlbl", type=str)
    parser.add_argument("--weight_dir", type=str)
    parser.add_argument("--gpu_id", type=str, default=0)
    parser.add_argument("--n_cores", type=int, default=1)
    parser.add_argument("--n_items", type=int)
    parser.add_argument("--filters", type=int)
    parser.add_argument("--batch_size", type=int)
    parser.add_argument("--patch_size", type=int)
    parser.add_argument("--n_resblocks", type=int)
    parser.add_argument("--kernel_size", type=int)
    parser.add_argument("--acq_dim", type=int)
    parser.add_argument("--n_aug_rots", type=int)
    parser.add_argument("--lr_axis", type=str, default=None)
    parser.add_argument("--k", type=float, default=None)
    parser.add_argument("--to_interp", type=str, default="True")
    parser.add_argument("--learning_rate", type=float, default=1e-4)
    parser.add_argument("--noise_level", type=float, default=None)
    parser.add_argument("--bg_tolerance", type=float, default=0.1)

    args = parser.parse_args()

    BATCH_SIZE = args.batch_size
    N_ITEMS = args.n_items
    N_STEPS = N_ITEMS // BATCH_SIZE

    n_aug_rots = args.n_aug_rots
    filters = args.filters
    n_resblocks = args.n_resblocks
    kernel_size = args.kernel_size
    ACQ_DIM = args.acq_dim
    to_interp = True if args.to_interp.lower() == "true" else False
    lr_axis = args.lr_axis
    k = args.k
    learning_rate = args.learning_rate
    noise_level = args.noise_level

    PATCH_SIZE = (args.patch_size, args.patch_size, 1)

    IN_FNAME = Path(args.infile)
    IN_LBL_FNAME = Path(args.inlbl)
    WEIGHT_DIR = Path(args.weight_dir)

    GPU_ID = args.gpu_id
    N_CORES = args.n_cores

    os.environ['CUDA_VISIBLE_DEVICES'] = GPU_ID

    if not WEIGHT_DIR.exists():
        WEIGHT_DIR.mkdir(parents=True)

    #################### DATA ####################

    dataset = ImageAndMask(
        img_fpath=IN_FNAME,
        lbl_fpath=IN_LBL_FNAME,
        patch_size=PATCH_SIZE,
        n_resblocks=n_resblocks,
        n_steps=N_ITEMS,
        n_rots=n_aug_rots,
        kernel_size=kernel_size,
        train=True,
        bg_tolerance=args.bg_tolerance,
    )

    n_lbl_classes = dataset.n_lbl_classes
    class_weights = torch.Tensor(dataset.class_weights).cuda()

    for mode in ['AA', 'SR']:

        dataset.set_mode(mode)
        data_loader = DataLoader(
            dataset,
            batch_size=BATCH_SIZE,
            shuffle=False,  # The dataset automatically shuffles
            pin_memory=True,
            num_workers=1,
        )

        #################### MODEL SETUP ####################

        train_st = time.time()

        model = EDSR(
            n_img_channels=1,
            n_lbl_classes=n_lbl_classes,
            kernel_size=kernel_size,
            n_resblocks=n_resblocks,
            filters=filters,
            padding_mode='zeros',
        ).cuda()

        # Initialize weights
#         model.apply(weights_init)

        opt = torch.optim.AdamW(model.parameters(), lr=learning_rate)
#         opt = torch.optim.Adam(model.parameters(), lr=learning_rate)

        scheduler = torch.optim.lr_scheduler.OneCycleLR(
            optimizer=opt,
            max_lr=learning_rate,
            total_steps=N_STEPS+1,  # handles the `step()` below
            cycle_momentum=True,
        )

#         multistep_steps = 3
#         scheduler = torch.optim.lr_scheduler.MultiStepLR(
#             optimizer=opt,
#             milestones=[
#                 int(m/multistep_steps * N_STEPS) for m in range(1, multistep_steps)
#             ],
#             gamma=0.1,
#         )

        opt.zero_grad()
        opt.step()

        scaler = torch.cuda.amp.GradScaler()
        img_loss_obj = l1_sobel_loss
#         gdl_loss_obj = GDiceLossV2(apply_nonlin=torch.nn.Softmax(dim=1))
        xentropy_loss_obj = torch.nn.CrossEntropyLoss(weight=class_weights)
        #FocalLoss(gamma=1, reduction='mean')
        # GDiceLossV2(apply_nonlin=torch.nn.Softmax(dim=1))
        #torch.nn.CrossEntropyLoss()

        #################### TRAIN ####################
        print("\n{} TRAINING {} NETWORK {}\n".format(
            "="*20,
            mode,
            "="*20,
        ))

        with tqdm(total=N_ITEMS) as pbar:

            for data in data_loader:
                opt.zero_grad()

                xs_img, xs_lbl, ys_img, ys_lbl = data

                ys_lbl = torch.argmax(ys_lbl, axis=1).long().cuda()

                with torch.cuda.amp.autocast():
                    ys_img_hat, ys_lbl_hat = model(
                        xs_img.float().cuda(),
                        xs_lbl.float().cuda(),
                    )

                    img_loss = img_loss_obj(ys_img_hat, ys_img.cuda())
                    lbl_loss = xentropy_loss_obj(ys_lbl_hat, ys_lbl)
#                     lbl_loss = gdl_loss_obj(ys_lbl_hat, ys_lbl)
#                     lbl_loss += xentropy_loss_obj(ys_lbl_hat, ys_lbl)
#                     lbl_loss /= 2

                lambda_img = 10
                lambda_lbl = 1
                loss = lambda_img * img_loss + lambda_lbl * lbl_loss

                scaler.scale(loss).backward()
                scaler.step(opt)
                scaler.update()

                scheduler.step()

                # Progress bar update
                pbar.set_postfix(
                    {
                        'loss': '{:.4f}'.format(loss.detach().cpu().numpy()),
                        'img_loss': '{:.4f}'.format(img_loss.detach().cpu().numpy()),
                        'lbl_loss': '{:.4f}'.format(lbl_loss.detach().cpu().numpy()),
                    }
                )
                pbar.update(BATCH_SIZE)

        #################### SAVE MODEL CONDITIONS ####################

        WEIGHT_FNAME = "{}_best_weights.h5".format(mode)
        WEIGHT_PATH = WEIGHT_DIR / WEIGHT_FNAME

        print()
        torch.save(
            {
                'model': model.state_dict(),
            },
            str(WEIGHT_PATH)
        )

    train_en = time.time()
    print(
        "\tElapsed time to finish {} training: {:.4f}s".format(
            mode,
            train_en-train_st,
        )
    )

    print("{} END TRAINING {}".format("="*20, "="*20))
