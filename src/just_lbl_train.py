import argparse
import nibabel as nib
import numpy as np
import torch
import time
import sys
import os

from models.edsr_one_input import *
from models.losses import *

from utils.mask_loader import *

from pathlib import Path
from tqdm import tqdm

np.random.seed(0)

# Optimize torch
torch.backends.cudnn.benchmark = True

if __name__ == '__main__':
    print("{a} BEGIN TRAINING {a}".format(a="="*20))

    #################### ARGUMENTS ####################
    parser = argparse.ArgumentParser()
    parser.add_argument("--inmask", type=str)
    parser.add_argument("--experiment", type=str, default=None)
    parser.add_argument("--weight_dir", type=str)
    parser.add_argument("--gpu_id", type=str, default=0)
    parser.add_argument("--n_cores", type=int, default=1)
    parser.add_argument("--n_items", type=int)
    parser.add_argument("--filters", type=int)
    parser.add_argument("--batch_size", type=int)
    parser.add_argument("--patch_size", type=int)
    parser.add_argument("--n_resblocks", type=int)
    parser.add_argument("--kernel_size", type=int)
    parser.add_argument("--acq_dim", type=int)
    parser.add_argument("--lr_axis", type=str, default=None)
    parser.add_argument("--k", type=float, default=None)
    parser.add_argument("--to_interp", type=str, default="True")
    parser.add_argument("--learning_rate", type=float, default=1e-4)
    parser.add_argument("--noise_level", type=float, default=None)

    args = parser.parse_args()

    BATCH_SIZE = args.batch_size
    N_ITEMS = args.n_items
    N_STEPS = N_ITEMS // BATCH_SIZE

    filters = args.filters
    n_resblocks = args.n_resblocks
    kernel_size = args.kernel_size
    ACQ_DIM = args.acq_dim
    to_interp = True if args.to_interp.lower() == "true" else False
    lr_axis = args.lr_axis
    k = args.k
    learning_rate = args.learning_rate
    noise_level = args.noise_level
    
    PATCH_SIZE = (args.patch_size, args.patch_size, 1)

    if args.experiment is not None:
        experiment = args.experiment
    else:
        experiment = "smore"
        
    IN_MASK_FNAME = Path(args.inmask)
    WEIGHT_DIR = Path(args.weight_dir)

    GPU_ID = args.gpu_id
    N_CORES = args.n_cores

    os.environ['CUDA_VISIBLE_DEVICES'] = GPU_ID

    if not WEIGHT_DIR.exists():
        WEIGHT_DIR.mkdir(parents=True)

    #################### DATA ####################

    dataset = MaskData(
        mask_fpath=IN_MASK_FNAME,
        patch_size=PATCH_SIZE,
        n_resblocks=n_resblocks,
        n_steps=N_ITEMS,
        n_rots=2,
        kernel_size=kernel_size,
        train=True,
    )
    
    n_mask_classes = dataset.n_mask_classes

    for mode in ['AA', 'SR']:

        dataset.set_mode(mode)
        data_loader = DataLoader(
            dataset,
            batch_size=BATCH_SIZE,
            shuffle=False,  # The dataset automatically shuffles
            pin_memory=True,
            num_workers=1,
        )

        #################### MODEL SETUP ####################

        train_st = time.time()

        model = EDSR(
            n_mask_classes=n_mask_classes,
            kernel_size=kernel_size,
            n_resblocks=n_resblocks,
            filters=filters,
            padding_mode='zeros',
        ).cuda()

        # Initialize weights
        model.apply(weights_init)

        opt = torch.optim.Adam(model.parameters(), lr=learning_rate)
        opt.zero_grad()
        opt.step()

        scaler = torch.cuda.amp.GradScaler()
        mask_loss_obj = torch.nn.CrossEntropyLoss(reduction='mean')

        #################### TRAIN ####################
        print("\n{} TRAINING {} NETWORK {}\n".format(
            "="*20,
            mode,
            "="*20,
        ))

        with tqdm(total=N_ITEMS) as pbar:

            for data in data_loader:
                opt.zero_grad()

                xs_mask, ys_mask = data

                with torch.cuda.amp.autocast():
                    ys_mask_hat = model(xs_mask.float().cuda())
                                        
                    mask_loss = mask_loss_obj(
                        ys_mask_hat,
                        torch.argmax(ys_mask, axis=1).long().cuda(),
                    )

                loss = mask_loss

                scaler.scale(loss).backward()
                scaler.step(opt)
                scaler.update()

                # Progress bar update
                pbar.set_postfix(
                    {
                        'loss': '{:.4f}'.format(loss.detach().cpu().numpy()),
                        'mask_loss': '{:.4f}'.format(mask_loss.detach().cpu().numpy()),
                    }
                )
                pbar.update(BATCH_SIZE)

        #################### SAVE MODEL CONDITIONS ####################

        WEIGHT_FNAME = "{}_best_weights.h5".format(mode)
        WEIGHT_PATH = WEIGHT_DIR / WEIGHT_FNAME

        print()
        torch.save(
            {
                'model': model.state_dict(),
            },
            str(WEIGHT_PATH)
        )

    train_en = time.time()
    print(
        "\tElapsed time to finish {} training: {:.4f}s".format(
            mode,
            train_en-train_st,
        )
    )

    print("{} END TRAINING {}".format("="*20, "="*20))
