import argparse
import nibabel as nib
import numpy as np
import torch
import time
import sys
import os

from models.explicit_model import *
from models.losses import *

from utils.img_and_lbl_loader import *

from pathlib import Path
from tqdm import tqdm

np.random.seed(0)

# Optimize torch
torch.backends.cudnn.benchmark = True

if __name__ == '__main__':
    print("{a} BEGIN TRAINING {a}".format(a="="*20))

    #################### ARGUMENTS ####################
    parser = argparse.ArgumentParser()
    parser.add_argument("--infile", type=str)
    parser.add_argument("--inlbl", type=str)
    parser.add_argument("--experiment", type=str, default=None)
    parser.add_argument("--weight_dir", type=str)
    parser.add_argument("--gpu_id", type=str, default=0)
    parser.add_argument("--n_cores", type=int, default=1)
    parser.add_argument("--n_items", type=int)
    parser.add_argument("--filters", type=int)
    parser.add_argument("--batch_size", type=int)
    parser.add_argument("--patch_size", type=int)
    parser.add_argument("--n_resblocks", type=int)
    parser.add_argument("--kernel_size", type=int)
    parser.add_argument("--acq_dim", type=int)
    parser.add_argument("--n_aug_rots", type=int)
    parser.add_argument("--lr_axis", type=str, default=None)
    parser.add_argument("--k", type=float, default=None)
    parser.add_argument("--to_interp", type=str, default="True")
    parser.add_argument("--learning_rate", type=float, default=1e-4)
    parser.add_argument("--noise_level", type=float, default=None)

    args = parser.parse_args()

    BATCH_SIZE = args.batch_size
    N_ITEMS = args.n_items
    N_STEPS = N_ITEMS // BATCH_SIZE

    n_aug_rots = args.n_aug_rots
    filters = args.filters
    n_resblocks = args.n_resblocks
    kernel_size = args.kernel_size
    ACQ_DIM = args.acq_dim
    to_interp = True if args.to_interp.lower() == "true" else False
    lr_axis = args.lr_axis
    k = args.k
    learning_rate = args.learning_rate
    noise_level = args.noise_level

    PATCH_SIZE = (args.patch_size, args.patch_size, 1)

    if args.experiment is not None:
        experiment = args.experiment
    else:
        experiment = "smore"
    IN_FNAME = Path(args.infile)
    IN_LBL_FNAME = Path(args.inlbl)
    WEIGHT_DIR = Path(args.weight_dir)

    GPU_ID = args.gpu_id
    N_CORES = args.n_cores

    os.environ['CUDA_VISIBLE_DEVICES'] = GPU_ID

    if not WEIGHT_DIR.exists():
        WEIGHT_DIR.mkdir(parents=True)

    #################### DATA ####################

    dataset = ImageAndMask(
        img_fpath=IN_FNAME,
        lbl_fpath=IN_LBL_FNAME,
        patch_size=PATCH_SIZE,
        n_resblocks=n_resblocks,
        n_steps=N_ITEMS,
        n_rots=n_aug_rots,
        kernel_size=kernel_size,
        train=True,
    )

    n_lbl_classes = dataset.n_lbl_classes

    for mode in ['AA', 'SR']:

        dataset.set_mode(mode)
        data_loader = DataLoader(
            dataset,
            batch_size=BATCH_SIZE,
            shuffle=False,  # The dataset automatically shuffles
            pin_memory=True,
            num_workers=1,
        )

        #################### MODEL SETUP ####################

        train_st = time.time()

        model = ExplicitModel(
            n_img_channels=1,
            n_lbl_channels=n_lbl_classes,
            kernel_size=kernel_size,
            n_resblocks=n_resblocks,
            filters=filters,
            padding_mode='zeros',
        ).cuda()

        opt = torch.optim.Adam(model.parameters(), lr=learning_rate)

        multistep_steps = 3
        scheduler = torch.optim.lr_scheduler.MultiStepLR(
            optimizer=opt,
            milestones=[
                int(m/multistep_steps * N_STEPS) for m in range(1, multistep_steps)
            ],
            gamma=0.1,
        )

        opt.zero_grad()
        opt.step()

        scaler = torch.cuda.amp.GradScaler()
        
        phi_inv_loss_obj = l1_sobel_loss
        f_loss_obj = torch.nn.CrossEntropyLoss()
        g_loss_obj = torch.nn.CrossEntropyLoss()
        psi_inv_loss_obj = torch.nn.CrossEntropyLoss()        
        psi_inv_loss_obj_2 = torch.nn.CrossEntropyLoss()

        #################### TRAIN ####################
        print("\n{} TRAINING {} NETWORK {}\n".format(
            "="*20,
            mode,
            "="*20,
        ))

        with tqdm(total=N_ITEMS) as pbar:

            for data in data_loader:
                opt.zero_grad()

                xs_lr, ys_lr, xs_hr, ys_hr = data
                
                with torch.cuda.amp.autocast():
                    res = model(
                        xs_lr.float().cuda(),
                        ys_lr.float().cuda(),
                    )
                    
                    ys_lr = torch.argmax(ys_lr, axis=1).long().cuda()
                    xs_hr = xs_hr.float().cuda()
                    ys_hr = torch.argmax(ys_hr, axis=1).long().cuda()
                    
                    hat_xs_hr, hat_ys_hr, hat_ys_lr, hat_ys_hr_2, hat_ys_hr_3 = res
                    
                    # x_lr -> x_hr
                    phi_inv_loss = phi_inv_loss_obj(hat_xs_hr, xs_hr)
                    
                    # x_lr -> x_hr -> y_hr
                    f_loss = f_loss_obj(hat_ys_hr, ys_hr)
                    
                    # x_lr -> y_lr
                    g_loss =  g_loss_obj(hat_ys_lr, ys_lr)
                    
                    # x_lr -> y_lr -> y_hr
                    psi_inv_loss = psi_inv_loss_obj(hat_ys_hr_2, ys_hr)
                    
                    # y_lr -> y_hr
                    psi_inv_loss_2 = psi_inv_loss_obj_2(hat_ys_hr_3, ys_hr)
                    

                loss = phi_inv_loss + f_loss + g_loss + psi_inv_loss + psi_inv_loss_2

                scaler.scale(loss).backward()
                scaler.step(opt)
                scaler.update()

                scheduler.step()

                # Progress bar update
                pbar.set_postfix(
                    {
                        'total': '{:.4f}'.format(loss.detach().cpu().numpy()),
                        'phi_inv': '{:.4f}'.format(phi_inv_loss.detach().cpu().numpy()),
                        'f': '{:.4f}'.format(f_loss.detach().cpu().numpy()),
                        'g': '{:.4f}'.format(g_loss.detach().cpu().numpy()),
                        'psi_inv': '{:.4f}'.format(psi_inv_loss.detach().cpu().numpy()),
                        'psi_inv_2': '{:.4f}'.format(psi_inv_loss_2.detach().cpu().numpy()),
                    }
                )
                pbar.update(BATCH_SIZE)

        #################### SAVE MODEL CONDITIONS ####################

        WEIGHT_FNAME = "{}_best_weights.h5".format(mode)
        WEIGHT_PATH = WEIGHT_DIR / WEIGHT_FNAME

        print()
        torch.save(
            {
                'model': model.state_dict(),
            },
            str(WEIGHT_PATH)
        )

    train_en = time.time()
    print(
        "\tElapsed time to finish {} training: {:.4f}s".format(
            mode,
            train_en-train_st,
        )
    )

    print("{} END TRAINING {}".format("="*20, "="*20))
