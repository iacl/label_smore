import argparse
import nibabel as nib
import numpy as np
import torch
import time
import sys
import os

from models.edsr_one_input import *
from models.losses import *

from utils.mask_loader import *
from utils.fba import *

from tqdm import tqdm
from pathlib import Path

np.random.seed(0)


def apply_model_volume_slices(mask_vol, model, batch_size):
    out_mask_vol = []

    i = 0

    with tqdm(total=len(mask_vol)) as pbar:
        while (i+batch_size) < len(mask_vol) + batch_size:
            batch_mask_slices = mask_vol[i:i+batch_size, ...]
            # Labels already have channels; no need to add channel dim
            # Instead, move channels to axis 2
            # shape is now (batch, channels, dim1, dim2)
            batch_mask_slices = batch_mask_slices.transpose(0, 3, 1, 2)
            
            # prepare for cuda
            batch_mask_slices = torch.from_numpy(
                batch_mask_slices).float().cuda()

            with torch.cuda.amp.autocast():
                mask_slices = model(batch_mask_slices)
                
                # apply softmax to mask slices along channel dim
                mask_slices = torch.nn.Softmax(dim=1)(mask_slices)
                mask_slices = mask_slices\
                    .detach()\
                    .cpu()\
                    .numpy()\
                    .astype(np.float32)

                # argmax along channel dim
                mask_slices = np.argmax(mask_slices, axis=1)
            # add to list
            out_mask_vol.extend(mask_slices)

            i += batch_size
            pbar.update(len(batch_mask_slices))

    out_mask_vol = np.array(out_mask_vol).squeeze()

    return out_mask_vol


def inv_get_n_rots(imgs_xyz, min_angle=0, max_angle=90, n_cores=1):
    # note: actually `n_rots` + 1 rotations are used
    n_rots = len(imgs_xyz) - 1

    if n_rots == 0:
        # no rotations
        return imgs_xyz
    angles = range(min_angle, max_angle+1, max_angle//n_rots)

    # move z axis to front to iterate over xy slices
    imgs_zxy = [img.transpose(2, 0, 1) for img in imgs_xyz]

    # Lambda functions to assist legibility of parallel call
    def slice_rot(sl, angle):
        if angle == 0:
            return sl
        elif angle == -90:
            return np.rot90(sl, k=-1)
        elif angle == -180:
            return np.rot90(sl, k=-2)
        elif angle == -270:
            return np.rot90(sl, k=-3)
        print("doing real rot")
        return rotate(sl, angle, reshape=False, mode='reflect')

    def apply_rot_to_vol(vol, angle): return np.array(
        [slice_rot(sl, -angle) for sl in vol]
    ).transpose(1, 2, 0)

    imgs_xyz = Parallel(n_jobs=n_cores, prefer="threads")(
        delayed(apply_rot_to_vol)(img_zxy, angle)
        for img_zxy, angle in zip(imgs_zxy, angles))

    return imgs_xyz


if __name__ == '__main__':

    main_st = time.time()
    print("\n{} BEGIN PREDICTION {}\n".format("="*20, "="*20))
    #################### ARGUMENTS ####################

    parser = argparse.ArgumentParser()
    parser.add_argument("--inmask", type=str)
    parser.add_argument("--outmask", type=str)
    parser.add_argument("--experiment", type=str, default=None)
    parser.add_argument("--weight_dir", type=str)
    parser.add_argument("--gpu_id", type=str)
    parser.add_argument("--patch_size", type=int)
    parser.add_argument("--batch_size", type=int)
    parser.add_argument("--n_resblocks", type=int)
    parser.add_argument("--n_rots", type=int)
    parser.add_argument("--filters", type=int)
    parser.add_argument("--kernel_size", type=int)
    parser.add_argument("--n_cores", type=int)
    parser.add_argument("--acq_dim", type=int)
    parser.add_argument("--lr_axis", type=str, default=None)
    parser.add_argument("--k", type=float, default=None)
    parser.add_argument("--img_p", type=str, default="inf")
    parser.add_argument("--mask_p", type=str, default="0")

    args = parser.parse_args()

    BATCH_SIZE = args.batch_size
    filters = args.filters
    n_resblocks = args.n_resblocks
    n_rots = args.n_rots
    kernel_size = args.kernel_size
    N_CORES = args.n_cores
    ACQ_DIM = args.acq_dim
    lr_axis = args.lr_axis
    k = args.k
    mask_p = args.mask_p
    if not mask_p == "inf" and not mask_p == "infinity":
        mask_p = int(mask_p)

    PATCH_SIZE = (args.patch_size, args.patch_size, 1)

    if args.experiment is not None:
        experiment = args.experiment
    else:
        experiment = "smore"

    IN_MASK_FPATH = Path(args.inmask)
    OUT_MASK_FNAME = Path(args.outmask)
    OUT_DIR = OUT_MASK_FNAME.parent
    WEIGHT_DIR = Path(args.weight_dir)

    GPU_ID = args.gpu_id
    os.environ['CUDA_VISIBLE_DEVICES'] = GPU_ID

    if not OUT_MASK_FNAME.parent.exists():
        OUT_MASK_FNAME.parent.mkdir(parents=True)

    #################### LOAD AND PROCESS DATA ####################

    dataset = MaskData(
        mask_fpath=IN_MASK_FPATH,
        patch_size=PATCH_SIZE,
        n_resblocks=n_resblocks,
        n_steps=None,
        n_rots=n_rots,
        kernel_size=kernel_size,
        train=False,
    )

    n_mask_classes = dataset.n_mask_classes

    masks_xyz = dataset.get_vols()
    masks_out = []

    #################### MODEL SETUP ####################

    WEIGHT_FNAME = "{}_best_weights.h5"

    AA_WEIGHT_PATH = WEIGHT_DIR / WEIGHT_FNAME.format('AA')
    aa_checkpoint = torch.load(AA_WEIGHT_PATH)
    aa_model = EDSR(
       n_mask_classes=n_mask_classes,
        kernel_size=kernel_size,
        n_resblocks=n_resblocks,
        filters=filters,
        padding_mode='zeros',
    ).cuda()
    aa_model.load_state_dict(aa_checkpoint['model'])

    SR_WEIGHT_PATH = WEIGHT_DIR / WEIGHT_FNAME.format('SR')
    sr_checkpoint = torch.load(SR_WEIGHT_PATH)
    sr_model = EDSR(
        n_mask_classes=n_mask_classes,
        kernel_size=kernel_size,
        n_resblocks=n_resblocks,
        filters=filters,
        padding_mode='zeros',
    ).cuda()
    sr_model.load_state_dict(sr_checkpoint['model'])

    #################### PREDICT ####################

    st = time.time()

    for mask_xyz in masks_xyz:

        # AA
        mask_yzx = mask_xyz.transpose(1, 2, 0, 3)
                
        print("Applying AA network...")
        mask_aa_yzx = apply_model_volume_slices(
            mask_vol=mask_yzx,
            model=aa_model,
            batch_size=BATCH_SIZE,
        )

        # transpose back
        mask_aa_xyz = mask_aa_yzx.transpose(2, 0, 1)
        mask_aa_xyz = mask_to_membership(mask_aa_xyz, dataset.n_mask_classes)
                
        # SR
        mask_aa_xzy = mask_aa_xyz.transpose(0, 2, 1, 3)

        print("Applying SR network...")
        mask_sr_xzy = apply_model_volume_slices(
            mask_vol=mask_aa_xzy,
            model=sr_model,
            batch_size=BATCH_SIZE,
        )

        # transpose back
        mask_sr_xyz = mask_sr_xzy.transpose(0, 2, 1)
        masks_out.append(mask_sr_xyz)

    en = time.time()
    print("Time for prediction : {:.4f}s".format(en-st))
    
    ########## Return to mask space ##########

    print("Returning to mask space...")
    masks_out = inv_get_n_rots(masks_out, max_angle=90, n_cores=N_CORES)
    masks_out = [dataset.crop(mask, dataset.bufs) for mask in masks_out]
    masks_out = [dataset.crop(mask, dataset.pads) for mask in masks_out]
    masks_out = [dataset.reorient(mask) for mask in masks_out]
    # membership before FBA
    membs_out = np.zeros(
        (len(masks_out), *masks_out[0].shape, n_mask_classes),
        dtype=np.float32,
    )
    for i, mask in enumerate(masks_out):
        memb = mask_to_membership(mask, dataset.n_mask_classes)
        
        for c in tqdm(range(n_mask_classes)):
            membs_out[i, ..., c] = memb[..., c]
    membs_fba = np.zeros_like(membs_out[0], dtype=np.float32)

    print("Channel-wise FBA...")
    for c in tqdm(range(n_mask_classes)):
        membs_fba[..., c] = fba(membs_out[..., c], p=mask_p, n_cores=N_CORES)

    mask_out = membership_to_mask(membs_fba)
    mask_out = mask_out.astype(np.int32)
    mask_out = dataset.invmap_labels(mask_out).astype(np.int32)

    ########## Save images ##########

    print("Saving images...")

    # Save Image
    dataset.write_nii(mask_out, OUT_MASK_FNAME)

    main_en = time.time()
    print("\n\nDONE\nElapsed time: {:.4f}s\n".format(main_en - main_st))
    print("\tWritten to:\n\t{}".format(OUT_MASK_FNAME))
    print("\n{} END PREDICTION {}\n".format("="*20, "="*20))
