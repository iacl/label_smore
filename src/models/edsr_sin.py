import torch.nn as nn
import torch
import numpy as np

torch.manual_seed(0)

# From torch source code
def _calculate_fan_in_and_fan_out(tensor):
    dimensions = tensor.dim()
    if dimensions < 2:
        raise ValueError("Fan in and fan out can not be computed for tensor with fewer than 2 dimensions")

    num_input_fmaps = tensor.size(1)
    num_output_fmaps = tensor.size(0)
    receptive_field_size = 1
    if tensor.dim() > 2:
        receptive_field_size = tensor[0][0].numel()
    fan_in = num_input_fmaps * receptive_field_size
    fan_out = num_output_fmaps * receptive_field_size

    return fan_in, fan_out

def _calculate_correct_fan(tensor, mode):
    mode = mode.lower()
    valid_modes = ['fan_in', 'fan_out']
    if mode not in valid_modes:
        raise ValueError("Mode {} not supported, please use one of {}".format(mode, valid_modes))

    fan_in, fan_out = _calculate_fan_in_and_fan_out(tensor)
    return fan_in if mode == 'fan_in' else fan_out

# Sitzmann initialization
def sitzmann_uniform_(tensor, is_first=False, omega_0=30.0):
    fan = _calculate_correct_fan(tensor, 'fan_in')
    
    if is_first:
        bound = 1 / fan
    else:
        bound = np.sqrt(6 / fan) / omega_0
        
    with torch.no_grad():
        return tensor.uniform_(-bound, bound)

def weights_init(layer):
    if isinstance(layer, nn.Conv2d):
        
        is_first = hasattr(layer, "is_first") and layer.is_first
        sitzmann_uniform_(
            layer.weight,
            is_first=is_first,
            omega_0=30.0,
        )
        nn.init.zeros_(layer.bias)

            
class Sin(nn.Module):
    def __init__(self):
        super(Sin, self).__init__()

    def forward(self, input):
        return torch.sin(input)
    
class Gain(nn.Module):
    def __init__(self, omega_0):
        super(Gain, self).__init__()
        self.omega_0 = omega_0
    def forward(self, input):
        return input * self.omega_0

class ResBlock(nn.Module):
    def __init__(
        self,
        in_channels,
        out_channels,
        kernel_size,
        res_scale,
        padding_mode,
        omega_0=30.0,
    ):
        super(ResBlock, self).__init__()

        padding = kernel_size // 2
#         padding = 0
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.omega_0 = omega_0

        self.body = nn.Sequential(
            nn.Conv2d(
                in_channels=self.in_channels,
                out_channels=self.out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            Sin(),
            nn.Conv2d(
                in_channels=self.out_channels,
                out_channels=self.out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
        )
        self.res_scale = res_scale

    def forward(self, x):
        out = self.body(x).mul(self.res_scale) * self.omega_0
        return out + x


class EDSR(nn.Module):
    def __init__(
        self,
        n_img_channels=1,
        n_mask_classes=2,
        kernel_size=3,
        n_resblocks=32,
        filters=256,
        padding_mode='zeros',
        omega_0=30.0,
    ):
        super(EDSR, self).__init__()
        padding = kernel_size // 2
        #padding = 0

        self.filters = filters
        self.n_resblocks = n_resblocks
        self.omega_0 = omega_0
        
        
        conv1_img = nn.Conv2d(
            in_channels=n_img_channels,
            out_channels=filters,
            kernel_size=kernel_size,
            stride=1,
            padding=padding,
            padding_mode=padding_mode,
            bias=True,
        )
        
        conv1_img.is_first = True
        
        conv1_lbl = nn.Conv2d(
            in_channels=n_mask_classes,
            out_channels=filters,
            kernel_size=kernel_size,
            stride=1,
            padding=padding,
            padding_mode=padding_mode,
            bias=True,
        )
        
        conv1_lbl.is_first = True

        # initial convolution
        self.img_head = nn.Sequential(
            conv1_img,
            Sin(),
            nn.Conv2d(
                in_channels=filters,
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            Sin(),
        )

        self.mask_head = nn.Sequential(
            conv1_lbl,
            Sin(),
            nn.Conv2d(
                in_channels=filters,
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            Sin(),
        )

        self.body = nn.Sequential(
            # Combine the concat img+mask and reduce from filters*2 to filters
            nn.Conv2d(
                in_channels=int(filters*2),
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            Sin(),
            *[
                ResBlock(
                    in_channels=filters,
                    out_channels=filters,
                    kernel_size=kernel_size,
                    res_scale=0.1,
                    padding_mode=padding_mode,
                ) for _ in range(n_resblocks)
            ],
            nn.Conv2d(
                in_channels=filters,
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            Sin(),
        )

        # img tail convolution
        self.img_tail = nn.Sequential(
            nn.Conv2d(
                in_channels=filters,
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            Sin(),
            nn.Conv2d(
                in_channels=filters,
                out_channels=n_img_channels,
                kernel_size=1,
                stride=1,
                bias=True,
            ),
            # No activation; we want it to be linear and clamp post-hoc
        )
        # mask tail convolution
        self.mask_tail = nn.Sequential(
            nn.Conv2d(
                in_channels=filters,
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            Sin(),
            nn.Conv2d(
                in_channels=filters,
                out_channels=n_mask_classes,
                kernel_size=1,
                stride=1,
                bias=True,
            ),
            # No activation bc softmax is done within the loss
        )

    def forward(self, img, mask):

        x_img = self.img_head(img)
        x_mask = self.mask_head(mask)

        # concatenate along channels
        cat = torch.cat((x_img, x_mask), axis=1)

        y = self.body(cat)

        img_pred = self.img_tail(x_img + x_mask + y) # n_img_channels
        mask_pred = self.mask_tail(x_img + x_mask + y) # n_mask_channels
        
        return img_pred, mask_pred
