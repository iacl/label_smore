import torch.nn as nn
import torch

torch.manual_seed(0)

class ResBlock(nn.Module):
    def __init__(
        self,
        in_channels,
        out_channels,
        kernel_size,
        res_scale,
        padding_mode='zeros',
    ):
        super(ResBlock, self).__init__()

        padding = kernel_size // 2
        self.in_channels = in_channels
        self.out_channels = out_channels

        self.body = nn.Sequential(
            nn.Conv2d(
                in_channels=self.in_channels,
                out_channels=self.out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.ReLU(),
            nn.Conv2d(
                in_channels=self.out_channels,
                out_channels=self.out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
        )
        self.res_scale = res_scale

    def forward(self, x):
        out = self.body(x).mul(self.res_scale)
        return out + x

class EDSR(nn.Module):
    def __init__(
        self,
        in_channels=1,
        out_channels=1,
        kernel_size=3,
        n_resblocks=32,
        filters=256,
        padding_mode='zeros',
    ):
        super(EDSR, self).__init__()
        padding = kernel_size // 2

        self.filters = filters
        self.n_resblocks = n_resblocks
        
        # initial convolution
        self.head = nn.Sequential(
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.ReLU(),
        )

        self.body = nn.Sequential(
            *[
                ResBlock(
                    in_channels=filters,
                    out_channels=filters,
                    kernel_size=kernel_size,
                    res_scale=0.1,
                    padding_mode=padding_mode,
                ) for _ in range(n_resblocks)
            ],
        )

        # img tail convolution
        self.tail = nn.Conv2d(
            in_channels=filters,
            out_channels=out_channels,
            kernel_size=1,
            stride=1,
            bias=True,
        )
        
    def forward(self, x):
        a = self.head(x)
        b = self.body(a)
        return self.tail(a + b)
    

class ExplicitModel(nn.Module):
    def __init__(
        self,
        n_img_channels=1,
        n_lbl_channels=2,
        kernel_size=3,
        n_resblocks=32,
        filters=256,
        padding_mode='zeros',
    ):
        super(ExplicitModel, self).__init__()
        
        # g maps X_LR to Y_LR
        self.g = EDSR(
            in_channels=n_img_channels,
            out_channels=n_lbl_channels,
            kernel_size=kernel_size,
            n_resblocks=n_resblocks,
            filters=filters,
            padding_mode=padding_mode,
        )
        
        # phi_inv maps X_LR to X_HR
        self.phi_inv = EDSR(
            in_channels=n_img_channels,
            out_channels=n_img_channels,
            kernel_size=kernel_size,
            n_resblocks=n_resblocks,
            filters=filters,
            padding_mode=padding_mode,
        )
        
        # psi_inv maps Y_LR to Y_HR
        self.psi_inv = EDSR(
            in_channels=n_lbl_channels,
            out_channels=n_lbl_channels,
            kernel_size=kernel_size,
            n_resblocks=n_resblocks,
            filters=filters,
            padding_mode=padding_mode,
        )
        
        # f maps X_HR to Y_HR
        self.f = EDSR(
            in_channels=n_img_channels,
            out_channels=n_lbl_channels,
            kernel_size=kernel_size,
            n_resblocks=n_resblocks,
            filters=filters,
            padding_mode=padding_mode,
        )
        
    def forward(self, x_lr, y_lr):
        # from x_lr
        
        # x_lr -> x_hr
        hat_x_hr = self.phi_inv(x_lr)
        # x_lr -> x_hr -> y_hr
        hat_y_hr = self.f(hat_x_hr)
        # x_lr -> y_lr
        hat_y_lr = self.g(x_lr)
        # x_lr -> y_lr -> y_hr
        hat_y_hr_2 = self.psi_inv(hat_y_lr)
        # y_lr -> y_hr
        hat_y_hr_3 = self.psi_inv(y_lr)

        return hat_x_hr, hat_y_hr, hat_y_lr, hat_y_hr_2, hat_y_hr_3