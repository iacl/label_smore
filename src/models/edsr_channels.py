import torch.nn as nn
import torch


def weights_init(layer):
    if isinstance(layer, nn.Conv2d):
        nn.init.xavier_uniform_(
            layer.weight,
            gain=nn.init.calculate_gain('tanh'),
        )
        nn.init.zeros_(layer.bias)


class ResBlock(nn.Module):
    def __init__(
        self,
        in_channels,
        out_channels,
        kernel_size,
        res_scale,
        padding_mode,
    ):
        super(ResBlock, self).__init__()

        padding = kernel_size // 2

        self.in_channels = in_channels
        self.out_channels = out_channels

        self.body = nn.Sequential(
            nn.Conv2d(
                in_channels=self.in_channels,
                out_channels=self.out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.ReLU(),
            nn.Conv2d(
                in_channels=self.out_channels,
                out_channels=self.out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
        )
        self.res_scale = res_scale

    def forward(self, x):
        out = self.body(x).mul(self.res_scale)
        return out + x


class EDSR(nn.Module):
    def __init__(
        self,
        n_img_channels=1,
        n_mask_channels=1,
        n_mask_classes=2,
        kernel_size=3,
        n_layers=68,
        filters=256,
        padding_mode='zeros',
    ):
        super(EDSR, self).__init__()
        padding = kernel_size // 2

        self.filters = filters
        self.n_img_channels = n_img_channels
        self.n_mask_channels = n_mask_channels
        self.n_mask_classes = n_mask_classes

        # initial convolution
        self.head = nn.Sequential(
            nn.Conv2d(
                in_channels=n_img_channels+n_mask_channels,
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.ReLU(),
        )

        n_res_blocks = (n_layers - 3) // 2
        self.body = nn.Sequential(
            *[
                ResBlock(
                    in_channels=filters,
                    out_channels=filters,
                    kernel_size=kernel_size,
                    res_scale=0.1,
                    padding_mode=padding_mode,
                ) for _ in range(n_res_blocks)
            ],
            nn.ReLU(),
        )

        # tail convolution
        self.tail = nn.Sequential(
            nn.Conv2d(
                in_channels=filters,
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.ReLU(),
            nn.Conv2d(
                in_channels=filters,
                out_channels=n_img_channels+n_mask_classes,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
        )

    def forward(self, img, mask):

        # concatenate along channels
        cat = torch.cat((img, mask), axis=1)

        x = self.head(cat)
        y = self.body(x)
        z = self.tail(x + y)

        i = self.n_img_channels
        j = i + self.n_mask_classes

        #img_pred = torch.tanh(z[:, :i, ...])
        img_pred = z[:, :i, ...]
        mask_pred = z[:, i:j, ...]

        return img_pred, mask_pred
