import torch.nn as nn


class UpsampleBlock(nn.Module):
    def __init__(
        self,
        filters,
        kernel_size,
        padding_mode,
    ):
        super(UpsampleBlock, self).__init__()

        padding = kernel_size // 2

        self.body = nn.Sequential(
            nn.ConvTranspose2d(
                in_channels=filters,
                out_channels=filters,
                kernel_size=kernel_size,
                stride=2,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.ReLU(inplace=True),
            nn.Conv2d(
                in_channels=filters,
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
        )

    def forward(self, x):
        return self.body(x)


class ResBlock(nn.Module):
    def __init__(
        self,
        in_channels,
        out_channels,
        low_level_filters,
        kernel_size,
        res_scale,
        padding_mode,
        n_blocks,
        block_idx=None,
    ):
        super(ResBlock, self).__init__()

        padding = kernel_size // 2

        self.block_idx = block_idx

        if self.block_idx is not None:
            self.fst_quar = n_blocks // 4
            self.snd_quar = self.fst_quar * 2
            self.thd_quar = self.fst_quar * 3

            # non-leading blocks have fixed n_filters
            if self.block_idx in range(0, self.fst_quar):
                self.in_channels = in_channels // 8
                self.out_channels = out_channels // 8
            elif self.block_idx in range(self.fst_quar, self.snd_quar):
                self.in_channels = in_channels // 4
                self.out_channels = out_channels // 4
            elif self.block_idx in range(self.snd_quar, self.thd_quar):
                self.in_channels = in_channels // 2
                self.out_channels = out_channels // 2
            else:
                self.in_channels = in_channels
                self.out_channels = out_channels

            # leading blocks compensate for varying filters
            if self.block_idx == 0:
                self.in_channels = low_level_filters
                self.out_channels = out_channels // 8
            elif self.block_idx == self.fst_quar:
                self.in_channels = in_channels // 8
                self.out_channels = out_channels // 4
            elif self.block_idx == self.snd_quar:
                self.in_channels = in_channels // 4
                self.out_channels = out_channels // 2
            elif self.block_idx == self.thd_quar:
                self.in_channels = in_channels // 2
                self.out_channels = out_channels

        self.body = nn.Sequential(
            nn.Conv2d(
                in_channels=self.in_channels,
                out_channels=self.out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.ReLU(inplace=True),
            nn.Conv2d(
                in_channels=self.out_channels,
                out_channels=self.out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
        )

        self.res_scale = res_scale

    def forward(self, x):
        out = self.body(x).mul(self.res_scale)
        # non-leading blocks are residual
        if self.block_idx not in [0, self.fst_quar, self.snd_quar, self.thd_quar]:
            return out + x
        # leading blocks are not residual
        else:
            return out


class EDSR(nn.Module):
    def __init__(
        self,
        n_channels,
        target_dim,
        n_upsamples,
        kernel_size=3,
        n_layers=67,
        filters=256,
        low_level_filters=16,
        padding_mode='zeros',
    ):
        super(EDSR, self).__init__()
        padding = kernel_size // 2

        self.target_dim = target_dim

        # initial convolution
        self.head = nn.Sequential(
            nn.Conv2d(
                in_channels=n_channels,
                out_channels=low_level_filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.ReLU(inplace=True),
        )

        # body loops for `n_layers-3` to account for initial + final convs
        # and divide by 2 bc each resblock has 2 convolution layers
        n_blocks = (n_layers - 3) // 2
        self.body = nn.Sequential(
            *[
                ResBlock(
                    in_channels=filters,
                    out_channels=filters,
                    low_level_filters=low_level_filters,
                    kernel_size=kernel_size,
                    res_scale=0.1,
                    n_blocks=n_blocks,
                    block_idx=block_idx,
                    padding_mode=padding_mode,
                ) for block_idx in range(n_blocks)
            ],
            nn.Conv2d(
                in_channels=filters,
                out_channels=low_level_filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.ReLU(inplace=True),
        )

        # tail convolution
        self.tail = nn.Sequential(
            nn.Conv2d(
                in_channels=low_level_filters,
                out_channels=low_level_filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.ReLU(inplace=True),
        )

        # upsample section
        self.upsample = nn.Sequential(
            *[
                UpsampleBlock(
                    filters=filters,
                    kernel_size=kernel_size,
                    padding_mode=padding_mode,
                ) for _ in range(n_upsamples)
            ]

        )

        # final conv
        self.finalize = nn.Conv2d(
            in_channels=filters,
            out_channels=1,
            kernel_size=kernel_size,
            stride=1,
            padding=padding,
            padding_mode=padding_mode,
            bias=True,
        )

    def forward(self, x):
        x = self.head(x)
        res = self.body(x)
        edsr_out = self.tail(res + x)
        upsampled_out = self.upsample(edsr_out)
        # crop feature map spatially
        cropped = upsampled_out[..., :self.target_dim[0], :self.target_dim[1]]
        return self.finalize(cropped)
