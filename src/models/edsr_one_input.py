import torch.nn as nn
import torch


def weights_init(layer):
    if isinstance(layer, nn.Conv2d):
        nn.init.kaiming_uniform_(
            layer.weight,
            nonlinearity='relu',
        )
        nn.init.zeros_(layer.bias)


class ResBlock(nn.Module):
    def __init__(
        self,
        in_channels,
        out_channels,
        kernel_size,
        res_scale,
        padding_mode,
    ):
        super(ResBlock, self).__init__()

        padding = kernel_size // 2
#         padding = 0
        self.in_channels = in_channels
        self.out_channels = out_channels

        self.body = nn.Sequential(
            nn.Conv2d(
                in_channels=self.in_channels,
                out_channels=self.out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.ReLU(),
            nn.Conv2d(
                in_channels=self.out_channels,
                out_channels=self.out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
        )
        self.res_scale = res_scale

    def forward(self, x):
        out = self.body(x).mul(self.res_scale)
        return out + x


class EDSR(nn.Module):
    def __init__(
        self,
        n_mask_classes=2,
        kernel_size=3,
        n_resblocks=32,
        filters=256,
        padding_mode='zeros',
    ):
        super(EDSR, self).__init__()
        padding = kernel_size // 2
        #padding = 0

        self.filters = filters
        self.n_resblocks = n_resblocks

        # initial convolution
        self.mask_head = nn.Sequential(
            nn.Conv2d(
                in_channels=n_mask_classes,
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.ReLU(),
            nn.Conv2d(
                in_channels=filters,
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.ReLU(),
        )

        self.body = nn.Sequential(
            nn.Conv2d(
                in_channels=int(filters*1),
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.ReLU(),
            *[
                ResBlock(
                    in_channels=filters,
                    out_channels=filters,
                    kernel_size=kernel_size,
                    res_scale=0.1,
                    padding_mode=padding_mode,
                ) for _ in range(n_resblocks)
            ],
            nn.Conv2d(
                in_channels=filters,
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.ReLU(),
        )

        # mask tail convolution
        self.mask_tail = nn.Sequential(
            nn.Conv2d(
                in_channels=filters,
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.ReLU(),
            nn.Conv2d(
                in_channels=filters,
                out_channels=n_mask_classes,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            # No activation bc softmax is done within the loss
        )

    def forward(self, mask):

        x_mask = self.mask_head(mask)

        y = self.body(x_mask)

        mask_pred = self.mask_tail(x_mask + y)  # n_mask_channels

        return mask_pred
