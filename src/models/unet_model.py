""" Full assembly of the parts to form the complete network """

'''
Modified from Milesial github
'''




import torch.nn.functional as F
from .unet_parts import *
class UNet(nn.Module):
    def __init__(
        self,
        n_img_channels,
        n_mask_channels,
        n_mask_classes,
        bilinear=True,
    ):
        super(UNet, self).__init__()
        self.n_img_channels = n_img_channels
        self.n_mask_channels = n_mask_channels
        self.n_mask_classes = n_mask_classes
        self.bilinear = bilinear

        self.inc = DoubleConv(n_img_channels+n_mask_channels, 64)
        self.down1 = Down(64, 128)
        self.down2 = Down(128, 256)
        self.down3 = Down(256, 512)
        factor = 2 if bilinear else 1
        self.down4 = Down(512, 1024 // factor)
        self.up1 = Up(1024, 512 // factor, bilinear)
        self.up2 = Up(512, 256 // factor, bilinear)
        self.up3 = Up(256, 128 // factor, bilinear)
        self.up4 = Up(128, 64, bilinear)
        self.outc = OutConv(64, n_img_channels+n_mask_classes)

    def forward(self, img, mask):

        x = torch.cat((img, mask), axis=1)

        x1 = self.inc(x)
        x2 = self.down1(x1)
        x3 = self.down2(x2)
        x4 = self.down3(x3)
        x5 = self.down4(x4)
        x = self.up1(x5, x4)
        x = self.up2(x, x3)
        x = self.up3(x, x2)
        x = self.up4(x, x1)
        logits = self.outc(x)

        i = self.n_img_channels
        j = i + self.n_mask_classes

        img_pred = logits[:, :i, ...]
        mask_pred = logits[:, i:j, ...]
        return img_pred, mask_pred
