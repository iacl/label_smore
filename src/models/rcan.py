'''
RCAN adapted from https://github.com/yulunzhang/RCAN/blob/master/RCAN_TrainCode/code/model/rcan.py
'''

import torch
import torch.nn as nn

def weights_init(layer):
    if isinstance(layer, nn.Conv2d):
        
#         nn.init.xavier_uniform_(
#             layer.weight,
#             gain=nn.init.calculate_gain('tanh'),
#         )
#         nn.init.zeros_(layer.bias)
        nn.init.he_uniform_(
            layer.weight,
            gain=nn.init.calculate_gain('relu'),
        )
        nn.init.zeros_(layer.bias)

class CALayer(nn.Module):
    def __init__(self, filters, reduction=16):
        super(CALayer, self).__init__()

        # global average pooling: feature --> point
        self.avg_pool = nn.AdaptiveAvgPool2d(1)

        # feature channel downscale and upscale --> channel weight
        self.conv_du = nn.Sequential(
            nn.Conv2d(filters, filters // reduction, 1, padding=0, bias=True),
            nn.ReLU(inplace=True),
            nn.Conv2d(filters // reduction, filters, 1, padding=0, bias=True),
            nn.Sigmoid()
        )

    def forward(self, x):
        y = self.avg_pool(x)
        y = self.conv_du(y)
        return x * y

class RCAB(nn.Module):
    def __init__(
        self,
        filters,
        kernel_size,
        reduction,
        bias=True,
        act=nn.ReLU(True),
        res_scale=1,
        padding_mode='zeros',
    ):

        super(RCAB, self).__init__()

        padding = kernel_size // 2
        self.res_scale = res_scale

        self.body = nn.Sequential(
            nn.Conv2d(
                in_channels=filters,
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            act,
            nn.Conv2d(
                in_channels=filters,
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            CALayer(filters, reduction),
        )

    def forward(self, x):
        res = self.body(x).mul(self.res_scale)
        res += x
        return res

class ResidualGroup(nn.Module):
    def __init__(
        self,
        filters,
        kernel_size,
        reduction,
        act,
        res_scale,
        padding_mode,
        n_resblocks,
    ):
        super(ResidualGroup, self).__init__()
        padding = kernel_size // 2

        self.body = nn.Sequential(
            *[
                RCAB(
                    filters,
                    kernel_size,
                    reduction,
                    bias=True,
                    act=act,
                    res_scale=res_scale,
                    padding_mode=padding_mode,
                ) for _ in range(n_resblocks)
            ],
            nn.Conv2d(
                in_channels=filters,
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
        )

    def forward(self, x):
        res = self.body(x)
        res += x
        return res

class RCAN(nn.Module):
    def __init__(
        self,
        n_img_channels=1,
        n_mask_channels=1,
        n_mask_classes=2,
        kernel_size=3,
        n_resgroups=10,
        n_resblocks=20,
        reduction=16,
        filters=128,
        padding_mode='zeros',
    ):
        super(RCAN, self).__init__()

        self.n_resgroups = n_resgroups
        self.n_resblocks = n_resblocks
        self.reduction = reduction

        self.filters = filters
        self.n_img_channels = n_img_channels
        self.n_mask_channels = n_mask_channels
        self.n_mask_classes = n_mask_classes

        self.kernel_size = kernel_size
        self.res_scale = 0.1
        act = nn.ReLU(True)
        padding = self.kernel_size // 2

        # initial convolution
        self.head = nn.Sequential(
            nn.Conv2d(
                in_channels=n_img_channels+n_mask_channels,
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            act,
        )

        # define body module
        self.body = nn.Sequential(
            *[
                ResidualGroup(
                    filters=filters,
                    kernel_size=self.kernel_size,
                    reduction=self.reduction,
                    act=act,
                    res_scale=self.res_scale,
                    padding_mode=padding_mode,
                    n_resblocks=self.n_resblocks,
                ) for _ in range(self.n_resgroups)
            ],
            nn.Conv2d(
                in_channels=filters,
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
        )

        # tail convolution
        self.tail = nn.Sequential(
            nn.Conv2d(
                in_channels=filters,
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            act,
            nn.Conv2d(
                in_channels=filters,
                out_channels=n_img_channels+n_mask_classes,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
        )

    def forward(self, img, mask):
        # concatenate along channels
        cat = torch.cat((img, mask), axis=1)

        x = self.head(cat)
        y = self.body(x)
        # long skip connection in-place
        y += x
        z = self.tail(y)

        i = self.n_img_channels
        j = i + self.n_mask_classes

        #img_pred = torch.tanh(z[:, :i, ...])
        img_pred = z[:, :i, ...]
        mask_pred = z[:, i:j, ...]

        return img_pred, mask_pred
