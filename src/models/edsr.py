import torch.nn as nn
import torch

# torch.manual_seed(0)

class CALayer(nn.Module):
    def __init__(self, filters, reduction=16):
        super(CALayer, self).__init__()

        # global average pooling: feature --> point
        self.avg_pool = nn.AdaptiveAvgPool2d(1)

        # feature channel downscale and upscale --> channel weight
        self.conv_du = nn.Sequential(
            nn.Conv2d(filters, filters // reduction, 1, padding=0, bias=True),
            nn.ReLU(inplace=True),
            nn.Conv2d(filters // reduction, filters, 1, padding=0, bias=True),
            nn.Sigmoid()
        )

    def forward(self, x):
        y = self.avg_pool(x)
        y = self.conv_du(y)
        return x * y

class ConvReLU(nn.Module):
    def __init__(
        self,
        in_channels,
        out_channels,
        kernel_size,
        padding_mode='zeros',
    ):
        super(ConvReLU, self).__init__()

        padding = kernel_size // 2

        self.body = nn.Sequential(
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.ReLU(),
        )

    def forward(self, x):
        return self.body(x)

class ResBlock(nn.Module):
    def __init__(
        self,
        in_channels,
        out_channels,
        kernel_size,
        res_scale,
        padding_mode='zeros',
    ):
        super(ResBlock, self).__init__()

        padding = kernel_size // 2
#         padding = 0
        self.in_channels = in_channels
        self.out_channels = out_channels

        self.body = nn.Sequential(
            nn.Conv2d(
                in_channels=self.in_channels,
                out_channels=self.out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.ReLU(),
            nn.Conv2d(
                in_channels=self.out_channels,
                out_channels=self.out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
        )
        self.res_scale = res_scale

    def forward(self, x):
        out = self.body(x).mul(self.res_scale)
        return out + x


class EDSR(nn.Module):
    def __init__(
        self,
        n_img_channels=1,
        n_lbl_classes=2,
        kernel_size=3,
        n_resblocks=32,
        filters=256,
        padding_mode='zeros',
    ):
        super(EDSR, self).__init__()
        padding = kernel_size // 2

        self.filters = filters
        self.n_resblocks = n_resblocks
        
        pre_shared_convs = 1
        post_attn_convs = 1
        
        # initial convolution
        self.img_head = nn.Sequential(
            nn.Conv2d(
                in_channels=n_img_channels,
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.ReLU(),
            *[
                ConvReLU(filters, filters, kernel_size)
                for _ in range(pre_shared_convs - 1)
            ],
        )

        self.lbl_head = nn.Sequential(
            nn.Conv2d(
                in_channels=n_lbl_classes,
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.ReLU(),
            *[
                ConvReLU(filters, filters, kernel_size)
                for _ in range(pre_shared_convs - 1)
            ],
        )
        
        self.body = nn.Sequential(
            # Combine the concat img+mask and reduce from filters*2 to filters
            nn.Conv2d(
                in_channels=int(filters*2),
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                bias=True,
            ),
            nn.ReLU(),
            *[
                ResBlock(
                    in_channels=filters,
                    out_channels=filters,
                    kernel_size=kernel_size,
                    res_scale=0.1,
                    padding_mode=padding_mode,
                ) for _ in range(n_resblocks)
            ],
            nn.Conv2d(
                in_channels=filters,
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.ReLU(),
        )
        
        self.img_attn = CALayer(filters=filters, reduction=1)

        # img tail convolution
        self.img_tail = nn.Sequential(
            *[
                ConvReLU(filters, filters, kernel_size)
                for _ in range(post_attn_convs - 1)
            ],
            nn.Conv2d(
                in_channels=filters,
                out_channels=n_img_channels,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            # No activation; linear
        )
        
        # label tail convolution
        self.lbl_attn = CALayer(filters=filters, reduction=1)
        
        self.lbl_tail = nn.Sequential(
            *[
                ConvReLU(filters, filters, kernel_size)
                for _ in range(post_attn_convs - 1)
            ],
            nn.Conv2d(
                in_channels=filters,
                out_channels=n_lbl_classes,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            # No activation bc softmax is done within the loss
        )

    def forward(self, img, lbl):

        x_img = self.img_head(img)
        x_lbl = self.lbl_head(lbl)

        # concatenate along channels
        cat = torch.cat((x_img, x_lbl), axis=1)

        y = self.body(cat)
        
        img_attn = self.img_attn(x_img + y)
        lbl_attn = self.lbl_attn(x_lbl + y)

        img_pred = self.img_tail(img_attn)
        lbl_pred = self.lbl_tail(lbl_attn)
        
        return img_pred, lbl_pred
