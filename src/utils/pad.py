import numpy as np

def get_pads(target_dim, d):
    p = (target_dim - d) // 2
    if (p*2+d) % 2 != 0:
        return (p, p + 1)
    return (p, p)

def target_pad(img, target_dims):

    pads = tuple(
        get_pads(t, d) if t != d else (0, 0)
        for t, d in zip(target_dims, img.shape)
    )
    
    return np.pad(
        img,
        pads,
        mode='reflect',
    ), pads
