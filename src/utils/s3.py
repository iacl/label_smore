'''
Compute no-reference sharpness metric S3
http://vision.eng.shizuoka.ac.jp/s3/

S3 expects a grayscale 2D image in [0, 255].
'''

import numpy as np
from skimage.data import camera
from scipy.signal.windows import hann
import matplotlib.pyplot as plt
import time
from tqdm import tqdm

def contrast_map_overlap(
    img,
    blk_size=8,
    t1=5,
    t2=2,
    b=0.7656, 
    k=0.0364, 
    gamma=2.2,
):
    '''
    Expectation: img \in [0, 255].
    
    
    for block x:
    
    l(x) = (b + kx)**gamma
    Normalize l(x) to [-127.5, 127.5]
    
    Contrast = zero if max(l(x)) - min(l(x)) <= T_1
    Contrast = zero if mean(l(x)) <= T_2
    
    '''
    # scale with heuristic luminance
    img_lum = (b + k*img) ** gamma

    d_blk = blk_size/2
    num_rows, num_cols = img_lum.shape
    
    cnt_map = np.zeros_like(img_lum)
    
    for r in range(0, int(num_rows - d_blk), int(d_blk)):
        for c in range(0, int(num_cols - d_blk), int(d_blk)):
            
            blk = img_lum[
                int(r):int(r+blk_size),
                int(c):int(c+blk_size),
            ]
            
            # normalize the mean
            m_lum = np.mean(blk)

            if m_lum > 127.5:
                blk = 255 - blk
                m_lum = np.mean(blk)
            
            
            zero_cond_1 = np.max(blk) - np.min(blk) <= t1
            zero_cond_2 = m_lum <= t2
            
            if zero_cond_1 or zero_cond_2:
                contrast = 0
            else:
                contrast = np.std(blk, ddof=1) / m_lum

            # clamp to t1
            if contrast > t1:
                contrast = t1
            
            # Scale down s.t. contrast map is in [0, 1]
            cnt_map[
                r:int(r+d_blk),
                c:int(c+d_blk),
            ] = contrast / t1
    return cnt_map

def spectral_map(img, pad_len=16):
    '''
    Spectral sharpness, slope of the power spectrum.
    aka "S1"
    '''
    
    blk_size = 32
    d_blk = blk_size / 4

    # paper uses symmetric padding of half of block size
    pads = tuple(
        (blk_size//2, blk_size//2)
        for _ in range(len(img.shape))
    )
    img = np.pad(img, pads, mode='symmetric')
    
    res = np.zeros_like(img, dtype=np.float32) - 100
    contrast_threshold = 0
    
    # loop over all blocks
    for r in tqdm(range(int(blk_size / 2), int(img.shape[0] - blk_size / 2)+1, int(d_blk))):
        for c in range(int(blk_size / 2), int(img.shape[1] - blk_size / 2)+1, int(d_blk)):
            # extract patch
            gry_blk = img[
                int(r-blk_size/2):int(r+blk_size/2),
                int(c-blk_size/2):int(c+blk_size/2),
            ]

            # get contrast map
            contrast_map = contrast_map_overlap(gry_blk)

            # get slopes
            if contrast_map.max() > contrast_threshold:
                val = blk_amp_spec_slope_eo_toy(gry_blk)
                val = val[0] # first index is the slope
                val = 1 - 1/(1 + np.exp(-3 * (val - 2)))
            else:
                val = 0
            # update contrast map
            res[
                int(r-d_blk/2):int(r+d_blk/2),
                int(c-d_blk/2):int(c+d_blk/2),
            ] = val
            
    # re-crop
    res = res[pad_len:-pad_len, pad_len:-pad_len]
    
    return res

def blk_amp_spec_slope_eo_toy(blk):
    n = blk.shape[0]
    # The scipy.signal.windows.hann window always has first and last zero-weighted samples
    # Thus to get a window without zeros of a certain length, first produce
    # a window of length n+2, then crop off the zeros
    wnd = hann(n+2)[1:-1][:, np.newaxis]
    wnd = np.dot(wnd, wnd.T)
    # product of image and hann window
    blk_wnd_prod = blk * wnd
    
    f_s, a_s = eo_polar_average(np.abs(np.fft.fftn(blk_wnd_prod)))

    p = np.polyfit(np.log(f_s), np.log(a_s), 1)
    
    return -p[0], p[1]

def eo_polar_average(data):
    
    tau = np.pi * 2
    dr = 360
    n = len(data)

    # DC is averaged by neighbors
    data[0, 0] = (data[1, 0] + data[0, 1]) / 2

    s = np.zeros(n//2 + 1, dtype=np.float64)
    
    rs = np.arange(1, n//2+1)

    zs = np.zeros_like(rs, dtype=np.float64)

    for ith in np.arange(dr):

        theta = ith / dr
        xs = rs * np.sin(tau * theta)
        ys = rs * np.cos(tau * theta)

        x1s = np.sign(xs) * np.floor(np.abs(xs))
        x2s = np.sign(xs) * np.ceil(np.abs(xs))
        y1s = np.sign(ys) * np.floor(np.abs(ys))
        y2s = np.sign(ys) * np.ceil(np.abs(ys))

        exs = np.abs(xs - x1s)
        eys = np.abs(ys - y1s)
        
        for i, x2 in enumerate(x2s):
            if x2 < 0:
                exs[i] = np.abs(xs[i] - x2)
                if x1s[i] < 0:
                    x1s[i] += n
                x2s[i] += n

        for i, y2 in enumerate(y2s):
            if y2 < 0:
                eys[i] = np.abs(ys[i] - y2)
                if y1s[i] < 0:
                    y1s[i] += n
                y2s[i] += n

        idx_11 = (np.array(x1s, dtype=np.int64), np.array(y1s, dtype=np.int64))
        idx_12 = (np.array(x1s, dtype=np.int64), np.array(y2s, dtype=np.int64))
        idx_21 = (np.array(x2s, dtype=np.int64), np.array(y1s, dtype=np.int64))
        idx_22 = (np.array(x2s, dtype=np.int64), np.array(y2s, dtype=np.int64))
    
        f11s = data[idx_11]
        f12s = data[idx_12]
        f21s = data[idx_21]
        f22s = data[idx_22]

        
        term1 = (f21s - f11s)*exs*(1-eys)
        term2 = (f12s-f11s)*(1-exs)*eys
        term3 = (f22s-f11s)*exs*eys
        term4 = f11s

        zs += term1 + term2 + term3 + term4

    s = zs / dr
    
    # prepend a 0
    s = np.concatenate([[0], s])
    
    f = np.linspace(0, 0.5, len(s))
    # skip first value
    f = f[1:]
    s = s[1:].astype(np.float32)
    

    return f, s

def spatial_map(img, pad_len):
    '''
    S2, spatial map by Total Variation
    '''
    blk_size = 8
    # paper uses symmetric padding
    pads = tuple(
        (pad_len, pad_len)
        for _ in range(len(img.shape))
    )
    img = np.pad(img, pads, mode='symmetric')
    
    res = np.zeros_like(img, dtype=np.float32)
    
    # loop over all blocks
    for r in tqdm(range(int(blk_size / 2), int(img.shape[0] - blk_size / 2)+1, int(blk_size))):
        for c in range(int(blk_size / 2), int(img.shape[1] - blk_size / 2)+1, int(blk_size)):
            
            # extract patch
            gry_blk = img[
                int(r-blk_size/2):int(r+blk_size/2),
                int(c-blk_size/2):int(c+blk_size/2),
            ]
            
            # Measure TV for every 2x2 block of gry_blk
            tv_tmp = []
            for i in range(blk_size-1):
                for j in range(blk_size-1):
                    tv = np.abs(gry_blk[i,j] - gry_blk[i,j+1]) \
                        + np.abs(gry_blk[i,j] - gry_blk[i+1,j]) \
                        + np.abs(gry_blk[i,j] - gry_blk[i+1,j+1]) \
                        + np.abs(gry_blk[i+1,j+1] - gry_blk[i+1,j]) \
                        + np.abs(gry_blk[i+1,j] - gry_blk[i,j+1]) \
                        + np.abs(gry_blk[i+1,j+1] - gry_blk[i,j+1])
                    
                    tv /= 255

                    tv_tmp.append(tv)
            tv_max = np.max(tv_tmp) / 4
            
            # update TV map
            res[
                int(r-blk_size/2):int(r+blk_size/2),
                int(c-blk_size/2):int(c+blk_size/2),
            ] = tv_max
            
    # re-crop
    res = res[pad_len:-pad_len, pad_len:-pad_len]
    return res

def s3_map(img):
    s1_map = spectral_map(img, 16)
    
    s2_map_8 = spatial_map(img, 8)
    s2_map_4 = spatial_map(img, 4)
    s2_map = np.max([s2_map_8, s2_map_4], axis=0)
    
    s1_map[np.where(s1_map < -99)] = 0
    s2_map[np.where(s2_map < -99)] = 0
    
    alpha = 0.5
    
    s3 = (s1_map ** alpha) * (s2_map ** (1-alpha))
    return s3

def s3_index(s3):
    s3_flat = s3.flatten()
    s3_flat.sort()
    s3_flat = s3_flat[::-1]
    n_take = int(np.floor(len(s3_flat) / 100))
    return s3_flat[:n_take].mean()