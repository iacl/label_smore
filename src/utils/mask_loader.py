import torch
import numpy as np
import nibabel as nib
import time
import gc
import sys
import itertools

from torch.utils.data import Dataset, DataLoader
from tqdm import tqdm

from .rotate import *
from .interpolate import *
from .degrade import *
from .membership import *
from .pad import *

def clear_memory():
    gc.collect()
    torch.cuda.empty_cache()

class MaskData(Dataset):
    def __init__(
        self,
        mask_fpath,
        patch_size,
        n_steps,
        n_resblocks,
        n_rots=0,
        kernel_size=3,
        mode='SR',
        train=True
    ):

        self.train = train

        min_angle = 0
        max_angle = 90
        if n_rots == 0:
            angles = [0]
        else:
            angles = range(min_angle, max_angle+1, max_angle//n_rots)

        self.patch_size = patch_size
        self.mode = mode
        self.n_steps = n_steps
        self.kernel_size = kernel_size
        self.n_resblocks = n_resblocks

        # Load nifti objs
        mask_obj = nib.load(mask_fpath)

        # Keep affines
        self.mask_affine = mask_obj.affine

        # Keep headers
        self.mask_header = mask_obj.header

        # Read acq dim
        self.acq_res = mask_obj.header.get_zooms()

        # NEW: Range for k for sampling scale
        k_radius = 2
        self.k_mid = round(max(self.acq_res) / min(self.acq_res), 2)
        self.k_mid = round(self.k_mid, 2)
        self.k_lo = self.k_mid - k_radius
        self.k_hi = self.k_mid + k_radius
        
        self.blur_k_mid = fwhm_needed(min(self.acq_res), max(self.acq_res))
        self.blur_k_lo = self.blur_k_mid - k_radius
        self.blur_k_hi = self.blur_k_mid + k_radius

        if train:
            # Select a new k
            self.k = np.random.uniform(self.k_lo, self.k_hi)
            self.blur_k = np.random.uniform(self.blur_k_lo, self.blur_k_hi)
        else:
            self.k = self.k_mid
            self.blur_k = self.blur_k_mid

        # Fpaths
        self.mask_fpath = mask_fpath

        # Load img and mask into RAM
        mask = mask_obj.get_fdata(dtype=np.float32)

        # Relabel mask
        self.label_map = index_labels(mask)
        self.n_mask_classes = len(self.label_map)
        mask = apply_relabel(mask, self.label_map)

        # For a somewhat-balanced label distribution, we'll keep an iterator
        # that cycles over all labels
        self.label_set = list(range(self.n_mask_classes))

        ########## PROCESS MASK ##########
        print("\n{a} PROCESSING MASK {a}".format(a="="*20))
        ##### 1. Orient s.t. z is the LR axis #####
        self.lr_axis = np.argmax(self.acq_res)
        if self.lr_axis == 0:
            mask = mask.transpose(1, 2, 0)
        elif self.lr_axis == 1:
            mask = mask.transpose(2, 0, 1)
        # if 2, no change

        ##### 2. Pad to square in-plane #####
        in_plane_shape = max(
            mask.shape[0],
            mask.shape[1],
        )
        target_shape = (in_plane_shape, in_plane_shape, mask.shape[2])
        mask, self.pads = target_pad(mask, target_shape)

        if self.train:
            ##### 3. Membership, rotate, alias, upsample #####
            print("\nProcessing for each of {} rotations...\n".format(n_rots))
            st = time.time()
            self.masks_rot = []
            self.masks_alias = []

            # Track mask indices for all labels
            # This will be a list of dicts
            self.masks_idx = []

            for angle in angles:
                mask_rot, mask_alias = self.process_mask(mask, angle)
                self.masks_rot.append(mask_rot)
                self.masks_alias.append(mask_alias)
                self.masks_idx.append(
                    {v: np.array(np.where(mask_rot == v))
                     for v in np.unique(mask_rot)}
                )
                clear_memory()

            en = time.time()
            print("\tElapsed time to process mask: {:.4f}s".format(en-st))
        else:
            ##### 3. Rotate, upsample #####
            print("\nProcessing for each of {} rotations...\n".format(n_rots))
            st = time.time()
            self.masks_rot = []

            for angle in angles:
                self.masks_rot.append(
                    self.process_mask(mask, angle, degrade=False))
                clear_memory()

            en = time.time()
            print("\tElapsed time to process mask: {:.4f}s".format(en-st))

    def __len__(self):
        return self.n_steps

    def set_mode(self, mode):
        self.mode = mode

    def __getitem__(self, i):
        '''
        To be used with DataLoader during training
        '''

        def safe_slice(st, en, p, dim):
            '''
            Avoid out-of-bounds
            '''
            if st < 0:
                st += p
                en += p
            elif en >= dim:
                st -= p
                en -= p

            return slice(st, en)

        # Select a new k
        self.k = np.random.uniform(self.k_lo, self.k_hi)
        self.blur_k = np.random.uniform(self.blur_k_lo, self.blur_k_hi)

        # Choose a random rotation
        rot_i = np.random.randint(0, len(self.masks_rot))

        # select patch by uniformly sampling a label
        # TODO: Sample weights inverse by label representation
        v = np.random.choice(self.label_set)
        selection = np.random.randint(0, self.masks_idx[rot_i][v].shape[1])
        target_idx = self.masks_idx[rot_i][v][..., selection]

        idx = tuple(
            safe_slice(m-(p//2), m+(p//2), p, s)
            if i < len(self.masks_rot[0].shape) - 1 else slice(m, m+1)
            for i, (m, p, s) in enumerate(
                zip(target_idx, self.patch_size, self.masks_rot[0].shape)
            )
        )

        # Both modes expect alias as input
        mask_patch_alias = self.masks_alias[rot_i][idx]
        # label to membership
        mask_patch_alias = mask_to_membership(mask_patch_alias.squeeze(), self.n_mask_classes)
        # channels first
        mask_patch_alias = mask_patch_alias.transpose(2, 0, 1)

        if self.mode == 'SR':  # SR learns alias -> HR
            mask_patch = self.masks_rot[rot_i][idx]
            mask_patch = mask_to_membership(mask_patch.squeeze(), self.n_mask_classes)
            mask_patch = mask_patch.transpose(2, 0, 1)
            return mask_patch_alias, mask_patch

        # Note: for AA mode, mask_blur is the same as `mask`.
        # This means that the mask branch in AA mode is learning the same problem as SR mode.

        # AA mode learns:
        # img_alias -> img_blur
        # mask_alias -> mask
        elif self.mode == 'AA':  # AA learns alias -> blur;
            mask_patch = self.masks_rot[rot_i][idx]
            mask_patch = mask_to_membership(mask_patch.squeeze(), self.n_mask_classes)
            mask_patch = mask_patch.transpose(2, 0, 1)
            return mask_patch_alias, mask_patch


    def process_mask(self, mask, angle, degrade=True):
        '''
        If degrade is False, only rotate + upsample.
        Otherwise:
        Process mask by:
            1. mask -> membership
            2. applying rotation
            3. aliasing (2)
            4. spline interpolate (2, 3)
            5. membership -> mask (4)
            5. return these two volumes
        '''
        print("{: <30}".format("Mask -> membership..."), end='')
        st = time.time()
        memb = mask_to_membership(mask, self.n_mask_classes)
        en = time.time()
        print("{:.4f}s".format(en-st))

        print("{: <30}".format("Rotating at {} degrees...".format(angle)), end='')
        st = time.time()
        memb_rot = rotate_vol_2D(memb, angle)
        en = time.time()
        print("{:.4f}s".format(en-st))

        if degrade:
            print("{: <30}".format("Degrading..."), end='')
            st = time.time()
            memb_alias = alias(memb_rot, k=self.k,
                               down_order=0, up_order=0, axis=0)
            en = time.time()
            print("{:.4f}s".format(en-st))

            print("{: <30}".format("Interpolating..."), end='')
            st = time.time()
            memb_rot = spline_interp(memb_rot, k=self.k, batch_size=8)
            memb_alias = spline_interp(memb_alias, k=self.k, batch_size=8)
            en = time.time()
            print("{:.4f}s".format(en-st))

            print("{: <30}".format("Memberships -> masks..."), end='')
            st = time.time()
            mask_rot = membership_to_mask(memb_rot)
            mask_alias = membership_to_mask(memb_alias)
            en = time.time()
            print("{:.4f}s".format(en-st))

            return mask_rot, mask_alias
        else:
            print("{: <30}".format("Interpolating..."), end='')
            st = time.time()
            memb_rot = spline_interp(memb_rot, k=self.k, batch_size=8)
            en = time.time()
            print("{:.4f}s".format(en-st))

            print("{: <30}".format("Memberships -> masks..."), end='')
            st = time.time()
            mask_rot = membership_to_mask(memb_rot)
            en = time.time()
            print("{:.4f}s".format(en-st))

            return mask_rot

    def get_vols(self):
        '''
        During test-time, return the list of rotated images.
        Pad each of them to mitigate edge effects
        '''
        buf = 2 * self.kernel_size
        target_dims = (
            self.masks_rot[0].shape[0] + buf,
            self.masks_rot[0].shape[1] + buf,
            self.masks_rot[0].shape[2] + buf,
        )

        masks = []
        for mask in self.masks_rot:
            mask_pad, bufs = target_pad(mask, target_dims)
            self.bufs = bufs
            mask_pad = mask_to_membership(mask_pad, self.n_mask_classes)
            masks.append(mask_pad)

        return masks

    def format_pads(self, pads):
        '''
        Turn pad amounts into appropriate slices
        and handle 0 pads as None slices
        '''
        st = pads[0] if pads[0] != 0 else None
        en = -pads[1] if pads[1] != 0 else None
        return slice(st, en)

    def crop(self, img, pads):
        ''' 
        remove buffer added in `get_vols()`

        Always nonzero so no issue
        '''
        crops = tuple(map(self.format_pads, pads))
        return img[crops]

    def reorient(self, img):
        '''
        Orient s.t. axes correspond with original axes
        '''

        if self.lr_axis == 0:
            return img.transpose(2, 0, 1)
        elif self.lr_axis == 1:
            return img.transpose(1, 2, 0)
        elif self.lr_axis == 2:
            return img

    def invmap_labels(self, mask):
        # Relabel mask
        inv_label_map = {v: k for k, v in self.label_map.items()}
        return apply_relabel(mask, inv_label_map)

    def write_nii(self, x, out_fpath):
        '''
        Write x as a nifti object.
        Uses the image's affine and header. This should be the same
        as the mask's; if not, there's an issue in the mask creation (outside of
        this program).
        '''
        # Update affine for super-resolved version
        new_scales = [1, 1, 1]
        new_scales[self.lr_axis] = 1/self.k
        new_scales = tuple(new_scales)
        new_affine = np.matmul(self.mask_affine, np.diag(new_scales + (1,)))
        out_obj = nib.Nifti1Image(
            x,
            affine=new_affine,
            header=self.mask_header,
        )
        nib.save(out_obj, out_fpath)
