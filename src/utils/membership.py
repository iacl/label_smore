import numpy as np
import torch
from tqdm import tqdm

def index_labels(lbl):
    '''
    Sometimes the labels are not sequentially counting up
    from 0, 1, 2, ..., n-1 for n labels.
    
    eg: SLANT has labels 0, 4, 11, 23, ... without a distinct pattern.
    
    This function indexes the labels to make them sequential,
    returning a dictionary mapping {original : indexed}.
    
    This dictionary can be inverted to map back to original labels.
    '''
    
    return {v:i for i, v in enumerate(np.unique(lbl))}

def apply_relabel(lbl, label_map):
    '''
    Relabels a lbl by the label map
    '''
    
    vals = np.unique(lbl)
    out = np.zeros_like(lbl, dtype=np.float32)
    
    for v in vals:
        out[np.where(lbl==v)] = label_map[v]
        
    return out
    

def lbl_to_membership(lbl, n_channels):
    '''
    Converts multi-label lbl to a multi-channel image
    Each channel is binary, each channel's index corresponds
    directly to the label
    '''
    out = np.zeros((*lbl.shape, n_channels), dtype=np.float32)
    
    for c_idx in range(n_channels):
        coords = np.where(lbl == c_idx)
        c = np.zeros_like(coords[0]) + c_idx
        out[(*coords, c)] = 1

    return out

def membership_to_lbl(membership):
    '''
    Converts multi-channel image to a single-channel
    multi-label lbl with integer labels starting from 0.
    '''

    return torch.argmax(
        torch.from_numpy(membership), 
        axis=-1
    ).detach().cpu().numpy().astype(np.float32)
