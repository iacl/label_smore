import numpy as np
from scipy import ndimage
from tqdm import tqdm
import torch

import torch.nn.functional as F

def spline_interp_old(x, k, order=3, axis=2):
    down_scale = tuple(
        k if i == axis else 1 for i in range(len(x.shape))
    )
    return ndimage.zoom(x, down_scale, order=order)


def spline_interp(vol, k, batch_size):
    '''
    
    `vol` should be of shape (H, W, D, C)

    Returns `out` of shape (H, W, D*k, C)
    '''
    
    h, w, d, c = vol.shape
    d_up = int(round(k * d))
    out = np.zeros((h, w, d_up, c), dtype=np.float32)
    
    for b in range(0, c, batch_size):
        st = b
        en = b + batch_size
        
        cur_batch = vol[..., st:en] # (H, W, D, C)
        cur_batch = cur_batch.transpose(3, 0, 1, 2) # (C, H, W, D)
        cur_batch = cur_batch[:, np.newaxis, ...] # (C, 1, H, W, D)
        
        up = F.interpolate(
            torch.from_numpy(cur_batch).cuda(), 
            size=(h, w, d_up), 
            mode='trilinear', 
            align_corners=True,
        ) # (C, 1, H, W, D)
        
        up = up.detach().cpu().numpy()[:, 0, ...] # (C, H, W, D)
        up = up.transpose(1, 2, 3, 0) # (H, W, D, C)
        
        out[..., st:en] = up
    
    return out