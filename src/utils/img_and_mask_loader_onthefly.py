import torch
import numpy as np
import nibabel as nib
import time
import gc
import sys
import itertools

from torch.utils.data import Dataset, DataLoader
from tqdm import tqdm

from .rotate import *
from .interpolate import *
from .degrade import *
from .membership import *
from .pad import *


def clear_memory():
    gc.collect()
    torch.cuda.empty_cache()


class ImageAndMask(Dataset):
    def __init__(
        self,
        img_fpath,
        mask_fpath,
        patch_size,
        n_steps,
        n_resblocks,
        n_rots=0,
        kernel_size=3,
        mode='SR',
        train=True
    ):

        self.train = train

        min_angle = 0
        max_angle = 90
        if n_rots == 0:
            angles = [0]
        else:
            angles = range(min_angle, max_angle+1, max_angle//n_rots)

        self.patch_size = patch_size
        self.mode = mode
        self.n_steps = n_steps
        self.kernel_size = kernel_size
        self.n_resblocks = n_resblocks

        # Load nifti objs
        nii_obj = nib.load(img_fpath)
        mask_obj = nib.load(mask_fpath)

        # Keep affines
        self.img_affine = nii_obj.affine
        self.mask_affine = mask_obj.affine

        # Keep headers
        self.img_header = nii_obj.header
        self.mask_header = mask_obj.header

        # Read acq dim
        self.acq_res = nii_obj.header.get_zooms()

        # NEW: Range for k for sampling scale
        k_radius = np.sqrt(2)
        self.k_mid = round(max(self.acq_res) / min(self.acq_res), 2)
        self.k_mid = round(self.k_mid, 2)
        self.k_lo = self.k_mid - k_radius
        self.k_hi = self.k_mid + k_radius

        blur_k_radius = np.sqrt(2)
        self.blur_k_mid = fwhm_needed(min(self.acq_res), max(self.acq_res))
        self.blur_k_lo = self.blur_k_mid - blur_k_radius
        self.blur_k_hi = self.blur_k_mid + blur_k_radius

        # Select a new k
        if train:
            self.k = np.random.uniform(self.k_lo, self.k_hi)
            self.blur_k = np.random.uniform(self.blur_k_lo, self.blur_k_hi)
        else:
            self.k = self.k_mid
            self.blur_k = self.blur_k_mid

        # Fpaths
        self.img_fpath = img_fpath
        self.mask_fpath = mask_fpath

        # Load img and mask into RAM
        img = nii_obj.get_fdata(dtype=np.float32)
        mask = mask_obj.get_fdata(dtype=np.float32)

        # Relabel mask
        self.label_map = index_labels(mask)
        self.n_mask_classes = len(self.label_map)
        mask = apply_relabel(mask, self.label_map)

        if img.shape != mask.shape:
            print("Shape mismatch between image and mask, exiting...")
            print("Image: {}\nMask: {}".format(img.shape, mask.shape))
            sys.exit()

        # For a somewhat-balanced label distribution, we'll keep an iterator
        # that cycles over all labels
        # here, `-1` will be a flag to switch to a bg-mask but non-zero img index
        self.label_set = list(range(self.n_mask_classes)) + \
            [-1] * self.n_mask_classes
#         self.label_set = [-1]

        ########## PREPROCESS IMAGE ##########
        print("\n{a} PROCESSING IMAGE {a}".format(a="="*20))

        ##### 1. Orient s.t. z is the LR axis #####
        self.lr_axis = np.argmax(self.acq_res)
        if self.lr_axis == 0:
            img = img.transpose(1, 2, 0)
        elif self.lr_axis == 1:
            img = img.transpose(2, 0, 1)
        # if 2, no change

        ##### 2. Pad to square in-plane #####
        in_plane_shape = max(
            img.shape[0],
            img.shape[1],
        )
        target_shape = (in_plane_shape, in_plane_shape, img.shape[2])
        img, self.pads = target_pad(img, target_shape)

        ##### 3. Add channel dimension #####
        img = img[..., np.newaxis]

        ##### 4. Normalize intensities #####
        print("{: <30}".format("Normalizing..."), end='')
        st = time.time()
        img, *self.inv_normalize_params = self.normalize(img, a=0, b=1)
        self.normalized_mean = img.mean()
        en = time.time()
        print("{:.4f}s".format(en-st))

        if self.train:
            ##### 4. Rotate  #####
            self.imgs_rot = []
            self.imgs_idx = []

            for angle in angles:
                print("{: <30}".format(
                    "Rotating at {} degrees...".format(angle)), end='')
                st = time.time()
                img_rot = rotate_vol_2D(img, angle)
                img_rot = img_rot.squeeze()
                self.imgs_rot.append(img_rot)
                en = time.time()
                print("{:.4f}s".format(en-st))
                
                # Track non-bg locations by q1 of its intensities
                q1 = np.quantile(img_rot, 0.05)
                self.imgs_idx.append(
                    np.array(np.where(q1 < img_rot)))

                clear_memory()

        else:
            print("\nProcessing for each of {} rotations...\n".format(n_rots))
            st = time.time()
            self.imgs_rot = []

            for angle in angles:
                self.imgs_rot.append(
                    self.process_img(img, angle, degrade=False))
                clear_memory()

            en = time.time()
            print("\tElapsed time to get process image: {:.4f}s".format(en-st))

        ########## PROCESS MASK ##########
        print("\n{a} PROCESSING MASK {a}".format(a="="*20))
        ##### 1. Orient s.t. z is the LR axis #####
        self.lr_axis = np.argmax(self.acq_res)
        if self.lr_axis == 0:
            mask = mask.transpose(1, 2, 0)
        elif self.lr_axis == 1:
            mask = mask.transpose(2, 0, 1)
        # if 2, no change

        ##### 2. Pad to square in-plane #####
        in_plane_shape = max(
            mask.shape[0],
            mask.shape[1],
        )
        target_shape = (in_plane_shape, in_plane_shape, mask.shape[2])
        mask, self.pads = target_pad(mask, target_shape)

        if self.train:
            ##### 3. Membership, rotate, alias, upsample #####
            self.masks_rot = []

            # Track mask indices for all labels
            # This will be a list of dicts
            self.masks_idx = []

            for angle in angles:
                print("{: <30}".format("Mask -> membership..."), end='')
                st = time.time()
                memb = mask_to_membership(mask, self.n_mask_classes)
                en = time.time()
                print("{:.4f}s".format(en-st))

                print("{: <30}".format(
                    "Rotating at {} degrees...".format(angle)), end='')
                st = time.time()
                memb_rot = rotate_vol_2D(memb, angle)
                en = time.time()
                print("{:.4f}s".format(en-st))

                print("{: <30}".format("Memberships -> masks..."), end='')
                st = time.time()
                mask_rot = membership_to_mask(memb_rot)
                en = time.time()
                print("{:.4f}s".format(en-st))

                mask_rot = mask_rot.squeeze()
                self.masks_rot.append(mask_rot)
                self.masks_idx.append(
                    {v: np.array(np.where(mask_rot == v))
                     for v in np.unique(mask_rot)}
                )
                clear_memory()

        else:
            ##### 3. Rotate, upsample #####
            print("\nProcessing for each of {} rotations...\n".format(n_rots))
            st = time.time()
            self.masks_rot = []

            for angle in angles:
                self.masks_rot.append(
                    self.process_mask(mask, angle, degrade=False))
                clear_memory()

            en = time.time()
            print("\tElapsed time to process mask: {:.4f}s".format(en-st))

    def __len__(self):
        return self.n_steps

    def set_mode(self, mode):
        self.mode = mode

    def __getitem__(self, i):
        '''
        To be used with DataLoader during training
        '''

        def safe_slice(st, en, p, dim):
            '''
            Avoid out-of-bounds
            '''
            if st < 0:
                st += p
                en += p
            elif en >= dim:
                st -= p
                en -= p

            return slice(st, en)

        # Choose a random rotation
        rot_i = np.random.randint(0, len(self.imgs_rot))

        # Select a new k
        self.k = np.random.uniform(self.k_lo, self.k_hi)
#         self.blur_k = self.k
        self.blur_k = np.random.uniform(self.blur_k_lo, self.blur_k_hi)

        # select patch by uniformly sampling a label
        # TODO: Sample weights inverse by label representation
        v = np.random.choice(self.label_set)
        if v != -1:
            selection = np.random.randint(0, self.masks_idx[rot_i][v].shape[1])
            target_idx = self.masks_idx[rot_i][v][..., selection]
        else:
            selection = np.random.randint(0, self.imgs_idx[rot_i].shape[1])
            target_idx = self.imgs_idx[rot_i][..., selection]

        idx = tuple(
            safe_slice(m-(p//2), m+(p//2), p, s)
            if i < len(self.masks_rot[0].shape) - 1 else slice(m, m+1)
            for i, (m, p, s) in enumerate(
                zip(target_idx, self.patch_size, self.masks_rot[0].shape)
            )
        )

        ########## SELECT PATCH  ##########

        # Target for SR mode
        img_patch = self.imgs_rot[rot_i][idx]
        mask_patch = self.masks_rot[rot_i][idx]
        mask_patch = mask_to_membership(mask_patch, self.n_mask_classes)
        mask_patch = mask_patch.squeeze()

        # Degrade img
        img_blur = blur(img_patch, blur_k=self.blur_k, axis=0)
        img_alias = alias(img_blur, k=self.k,
                          down_order=1, up_order=1, axis=0)

        # Degrade mask
        mask_blur = blur(mask_patch, blur_k=self.blur_k, axis=0)
        mask_alias = alias(mask_blur, k=self.k,
                           down_order=0, up_order=0, axis=0)
        
        mask_blur = membership_to_mask(mask_blur)
        mask_blur = mask_to_membership(mask_blur, self.n_mask_classes)
        mask_blur = mask_blur.squeeze()

        mask_alias = membership_to_mask(mask_alias)
        mask_alias = mask_to_membership(mask_alias, self.n_mask_classes)
        mask_alias = mask_alias.squeeze()

        # Channels first
        img_patch = img_patch.transpose(2, 0, 1)
        mask_patch = mask_patch.transpose(2, 0, 1)
        img_blur = img_blur.transpose(2, 0, 1)
        img_alias = img_alias.transpose(2, 0, 1)
        mask_blur = mask_blur.transpose(2, 0, 1)
        mask_alias = mask_alias.transpose(2, 0, 1)

        if self.mode == 'SR':
            return img_alias, mask_alias, img_patch, mask_patch
        else:
            return img_alias, mask_alias, img_blur, mask_blur
#             return img_alias, mask_alias, img_blur, mask_patch

        
    def process_img(self, img, angle, degrade=True):
        '''

        If degrade is False, only rotate + upsample.
        Otherwise:

        Process image by:
            1. applying rotation
            2. blurring (1)
            3. aliasing (2)
            4. spline interpolate (1, 2, 3)
            5. return these three volumes
        '''
        print("{: <30}".format("Rotating at {} degrees...".format(angle)), end='')
        st = time.time()
        img_rot = rotate_vol_2D(img, angle)
        en = time.time()
        print("{:.4f}s".format(en-st))

        if degrade:
            print("{: <30}".format("Degrading..."), end='')
            st = time.time()
            img_blur = blur(img_rot, blur_k=self.blur_k, axis=0)
            img_alias = alias(img_blur, k=self.k,
                              down_order=1, up_order=1, axis=0)
            en = time.time()
            print("{:.4f}s".format(en-st))

            print("{: <30}".format("Interpolating..."), end='')
            st = time.time()
#             img_rot = spline_interp_old(img_rot, k=self.k, axis=2)
#             img_blur = spline_interp_old(img_blur, k=self.k, axis=2)
#             img_alias = spline_interp_old(img_alias, k=self.k, axis=2)
            img_rot = spline_interp(img_rot, k=self.k, batch_size=1)
            img_blur = spline_interp(img_blur, k=self.k, batch_size=1)
            img_alias = spline_interp(img_alias, k=self.k, batch_size=1)
            en = time.time()
            print("{:.4f}s".format(en-st))

            # Discard channel dimension
            img_rot = img_rot[..., 0]
            img_blur = img_blur[..., 0]
            img_alias = img_alias[..., 0]

            return img_rot, img_blur, img_alias
        else:
            print("{: <30}".format("Interpolating..."), end='')
            st = time.time()
#             img_rot = spline_interp_old(img_rot, k=self.k, axis=2)
            img_rot = spline_interp(img_rot, k=self.k, batch_size=1)
            en = time.time()
            print("{:.4f}s".format(en-st))

            # Discard channel dimension
            img_rot = img_rot[..., 0]

            return img_rot

    def process_mask(self, mask, angle, degrade=True):
        '''
        If degrade is False, only rotate + upsample.
        Otherwise:
        Process mask by:
            1. mask -> membership
            2. applying rotation
            3. aliasing (2)
            4. spline interpolate (2, 3)
            5. membership -> mask (4)
            5. return these two volumes
        '''
        print("{: <30}".format("Mask -> membership..."), end='')
        st = time.time()
        memb = mask_to_membership(mask, self.n_mask_classes)
        en = time.time()
        print("{:.4f}s".format(en-st))

        print("{: <30}".format("Rotating at {} degrees...".format(angle)), end='')
        st = time.time()
        memb_rot = rotate_vol_2D(memb, angle)
        en = time.time()
        print("{:.4f}s".format(en-st))

        if degrade:
            print("{: <30}".format("Degrading..."), end='')
            st = time.time()
            memb_alias = alias(memb_rot, k=self.k,
                               down_order=0, up_order=0, axis=0)
            en = time.time()
            print("{:.4f}s".format(en-st))

            print("{: <30}".format("Interpolating..."), end='')
            st = time.time()
            memb_rot = spline_interp(memb_rot, k=self.k, batch_size=8)
            memb_alias = spline_interp(memb_alias, k=self.k, batch_size=8)
            en = time.time()
            print("{:.4f}s".format(en-st))

            print("{: <30}".format("Memberships -> masks..."), end='')
            st = time.time()
            mask_rot = membership_to_mask(memb_rot)
            mask_alias = membership_to_mask(memb_alias)
            en = time.time()
            print("{:.4f}s".format(en-st))

            return mask_rot, mask_alias
        else:
            print("{: <30}".format("Interpolating..."), end='')
            st = time.time()
            memb_rot = spline_interp(memb_rot, k=self.k, batch_size=8)
            en = time.time()
            print("{:.4f}s".format(en-st))

            print("{: <30}".format("Memberships -> masks..."), end='')
            st = time.time()
            mask_rot = membership_to_mask(memb_rot)
            en = time.time()
            print("{:.4f}s".format(en-st))

            return mask_rot

    def get_vols(self):
        '''
        During test-time, return the list of rotated images.
        Pad each of them to mitigate edge effects
        '''
        buf = 2 * self.kernel_size
        target_dims = (
            self.imgs_rot[0].shape[0] + buf,
            self.imgs_rot[0].shape[1] + buf,
            self.imgs_rot[0].shape[2] + buf,
        )

        imgs = []
        for img in self.imgs_rot:
            img_pad, bufs = target_pad(img, target_dims)
            self.bufs = bufs
            imgs.append(img_pad)

        masks = []
        for mask in self.masks_rot:
            mask_pad, bufs = target_pad(mask, target_dims)
            self.bufs = bufs
            mask_pad = mask_to_membership(mask_pad, self.n_mask_classes)
            masks.append(mask_pad)

        return imgs, masks

    def format_pads(self, pads):
        '''
        Turn pad amounts into appropriate slices
        and handle 0 pads as None slices
        '''
        st = pads[0] if pads[0] != 0 else None
        en = -pads[1] if pads[1] != 0 else None
        return slice(st, en)

    def crop(self, img, pads):
        ''' 
        remove buffer added in `get_vols()`

        Always nonzero so no issue
        '''
        crops = tuple(map(self.format_pads, pads))
        return img[crops]

    def reorient(self, img):
        '''
        Orient s.t. axes correspond with original axes
        '''

        if self.lr_axis == 0:
            return img.transpose(2, 0, 1)
        elif self.lr_axis == 1:
            return img.transpose(1, 2, 0)
        elif self.lr_axis == 2:
            return img

    def invmap_labels(self, mask):
        # Relabel mask
        inv_label_map = {v: k for k, v in self.label_map.items()}
        return apply_relabel(mask, inv_label_map)

    def normalize(self, x, a=-1, b=1):
        orig_mean = x.mean()
        orig_std = x.std()
        x = (x - orig_mean) / orig_std
        squash_min = x.min()
        squash_max = x.max()
        x = a + ((x - squash_min)*(b-a)) / (squash_max - squash_min)
        return x, squash_min, squash_max, orig_mean, orig_std, a, b
#         orig_max = x.max()
#         orig_min = x.min()
#         x = ((x - orig_min) * (b - a)) / (orig_max - orig_min) + a
#         return x, orig_min, orig_max, a, b

    def inv_normalize(self, y, inv_norm_params=None):
        if inv_norm_params is None:
            squash_min, squash_max, orig_mean, orig_std, a, b = self.inv_normalize_params
        else:
            squash_min, squash_max, orig_mean, orig_std, a, b = inv_norm_params

        tmp = y - a
        tmp *= squash_max - squash_min
        tmp /= b - a
        tmp += squash_min
        tmp *= orig_std
        tmp += orig_mean

        return tmp
#         if inv_norm_params is None:
#             orig_min, orig_max, a, b = self.inv_normalize_params
#         else:
#             orig_min, orig_max, a, b = inv_norm_params
#         tmp = y - a
#         tmp = y * (orig_max - orig_min)
#         tmp /= (b - a)
#         tmp += orig_min
#         return tmp

    def write_nii(self, x, out_fpath):
        '''
        Write x as a nifti object.
        Uses the image's affine and header. This should be the same
        as the mask's; if not, there's an issue in the mask creation (outside of
        this program).
        '''
        # Update affine for super-resolved version
        new_scales = [1, 1, 1]
        new_scales[self.lr_axis] = 1/self.k
        new_scales = tuple(new_scales)
        new_affine = np.matmul(self.img_affine, np.diag(new_scales + (1,)))
        out_obj = nib.Nifti1Image(
            x,
            affine=new_affine,
            header=self.img_header,
        )
        nib.save(out_obj, out_fpath)
