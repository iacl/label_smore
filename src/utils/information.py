import numpy as np
from skimage.util.shape import view_as_windows
from collections import defaultdict

def bytes_to_patch(byte_arr, dtype, patch_size):
    return np.frombuffer(byte_arr, dtype=dtype).reshape(patch_size)

def estimate_density(x, patch_size):
    # Look-up table for probabilities
    lut = defaultdict(int)
    
    # Indices as views
    views = view_as_windows(x, patch_size)
    indices = np.ndindex(views.shape[:len(patch_size)])
    it = iter(indices)
    
    # First, get histogram counts
    for idx in it:
        patch = views[idx]
        # Skip empty patches
        if patch.sum() != 0:
            lut[patch.tobytes()] += 1
            
    # Normalize LUT
    N = sum(lut.values())
    for patch in lut.keys():
        lut[patch] /= N
        
    return lut

def estimate_joint_density(x, y, patch_size):
    # Look-up table for probabilities
    lut = defaultdict(int)
    
    # Indices as views
    views_x = view_as_windows(x, patch_size)
    views_y = view_as_windows(y, patch_size)
    indices = np.ndindex(views_x.shape[:len(patch_size)])
    it = iter(indices)
    
    # First, get histogram counts
    for idx in it:
        patch_x = views_x[idx]
        patch_y = views_y[idx]
        patch = np.concatenate([patch_x, patch_y])
        
        # Skip empty patches
        if patch.sum() != 0:
            lut[patch.tobytes()] += 1
            
    # Normalize LUT
    N = sum(lut.values())
    for patch in lut.keys():
        lut[patch] /= N
        
    return lut

def entropy(lut):
    epsilon = 1e-16
    probs = np.array(list(lut.values())) + epsilon
    return -np.sum(probs * np.log2(probs))

def mutual_information(x, y, patch_size):
    # Marginal and joint densities
    p_x = estimate_density(x, patch_size)
    p_y = estimate_density(y, patch_size)
    p_xy = estimate_joint_density(x, y, patch_size)
    
    H_x = entropy(p_x)
    H_y = entropy(p_y)
    H_xy = entropy(p_xy)
    
    return H_x + H_y - H_xy