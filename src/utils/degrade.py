import numpy as np
from skimage import transform
from scipy import ndimage
from scipy.signal import windows
from tqdm import tqdm
from kornia.filters import SpatialGradient
import torch

def std_to_fwhm(sigma):
    return 2 * np.sqrt(2 * np.log(2)) * sigma

def fwhm_to_std(gamma):
    return gamma / (2 * np.sqrt(2 * np.log(2)))

def fwhm_needed(fwhm_hr, fwhm_lr):
    return np.sqrt(fwhm_lr**2 - fwhm_hr**2)

def blur(x, blur_k, axis=0):
    kernel = windows.gaussian(
        int(2*round(blur_k)+1),
        fwhm_to_std(blur_k),
    )
    kernel /= kernel.sum() # remove gain
    blurred = ndimage.convolve1d(x, kernel, mode='nearest', axis=axis)

    return blurred

def alias(x, k, down_order, up_order, axis):

    down_scale = [1/k if i == axis else 1 for i in range(len(x.shape))][:-1]
    
    *dims, c = x.shape
    
    out = np.zeros_like(x, dtype=np.float32)

    for cur_c in range(c):
        x_ds = transform.rescale(
            image=x[..., cur_c],
            scale=down_scale,
            mode='edge',
            order=down_order,
            preserve_range=True,
            anti_aliasing=False,
        )

        # To upsample up to the correct dimension, need to calculate
        # a new zoom_factor
        target_shape = x[..., cur_c].shape
        
        zoom_factor = [a/b for a, b in zip(target_shape, x_ds.shape)]
        
        x_us = ndimage.zoom(
            x_ds,
            zoom=zoom_factor,
            order=up_order,
            mode='nearest',
            prefilter=True,
        )
        
#         x_us = transform.resize(
#             image=x_ds,
#             output_shape=x[..., cur_c].shape,
#             mode='edge',
#             order=up_order,
#             preserve_range=True,
#             anti_aliasing=False,
#         )
        
        out[..., cur_c] = x_us.astype(np.float32)

    return out


def alias_3d(x, k, down_order, up_order, axis):

    down_scale = [1/k if i == axis else 1 for i in range(len(x.shape))][:-1]
    
    h, w, d, c = x.shape
    
    out = np.zeros_like(x, dtype=np.float32)

    for cur_c in range(c):
        x_ds = transform.rescale(
            image=x[..., cur_c],
            scale=down_scale,
            mode='edge',
            order=down_order,
            preserve_range=True,
            anti_aliasing=False,
        )

        # To upsample up to the correct dimension, need to calculate
        # a new zoom_factor
        target_shape = x[..., cur_c].shape
        
        zoom_factor = [a/b for a, b in zip(target_shape, x_ds.shape)]
        
        x_us = ndimage.zoom(
            x_ds,
            zoom=zoom_factor,
            order=up_order,
            mode='nearest',
            prefilter=True,
        )
        
#         x_us = transform.resize(
#             image=x_ds,
#             output_shape=x[..., cur_c].shape,
#             mode='edge',
#             order=up_order,
#             preserve_range=True,
#             anti_aliasing=False,
#         )
        
        out[..., cur_c] = x_us.astype(np.float32)

    return out

def measure_aliasing(img):
    """
    Measure aliasing by computing the gradient along x
    with a Sobel filter, then take the average x-direction std
    
    Higher value means more variance in the x-direction,
    and we can assume this means more aliasing.
    
    Returns a single float per image
    
    Input: img: np.array of shape CxHxW
    Output: float
    """
    sg = SpatialGradient()
    edge_yx = sg(torch.from_numpy(img[None, ...]))
    edge_y = edge_yx[:, :, 0, :, :]
    edge_x = edge_yx[:, :, 1, :, :]
    
    return edge_x.std(axis=2).mean().item()
    

def degrade(img, blur_k, k, down_order, up_order, axis):
    blurred = blur(img, blur_k, axis)
    aliased = alias(blurred, k, down_order, up_order, axis)
    return blurred, aliased
    
