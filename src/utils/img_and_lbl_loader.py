import torch
import numpy as np
import nibabel as nib
import time
import gc
import sys
import itertools

from torch.utils.data import Dataset, DataLoader
from tqdm import tqdm

from .rotate import *
from .interpolate import *
from .degrade import *
from .membership import *
from .pad import *


def clear_memory():
    gc.collect()
    torch.cuda.empty_cache()


class ImageAndMask(Dataset):
    def __init__(
        self,
        img_fpath,
        lbl_fpath,
        patch_size,
        n_steps,
        n_resblocks,
        n_rots=0,
        kernel_size=3,
        mode='SR',
        train=True,
        bg_tolerance=0.1,
    ):

        self.train = train

        min_angle = 0
        max_angle = 90
        if n_rots == 0:
            angles = [0]
        else:
            angles = range(min_angle, max_angle+1, max_angle//n_rots)

        self.patch_size = patch_size
        self.mode = mode
        self.n_steps = n_steps
        self.kernel_size = kernel_size
        self.n_resblocks = n_resblocks

        # Load nifti objs
        nii_obj = nib.load(img_fpath)
        lbl_obj = nib.load(lbl_fpath)

        # Keep affines
        self.img_affine = nii_obj.affine
        self.lbl_affine = lbl_obj.affine

        # Keep headers
        self.img_header = nii_obj.header
        self.lbl_header = lbl_obj.header

        # Read acq dim
        self.acq_res = nii_obj.header.get_zooms()

        # OLD: point for k
        """
        self.k = round(max(self.acq_res) / min(self.acq_res), 2)
        self.k = round(self.k, 2)
        self.blur_k = fwhm_needed(min(self.acq_res), max(self.acq_res))
        """
                
        
        self.k_mid = round(max(self.acq_res) / min(self.acq_res), 2)
        self.k_mid = round(self.k_mid, 2)
        
        # NEW: Range for k for sampling scale
        k_radius = 0.2 * self.k_mid#np.sqrt(2)
        
        self.k_lo = self.k_mid - k_radius
        self.k_hi = self.k_mid + k_radius
        
        self.blur_k_mid = fwhm_needed(min(self.acq_res), max(self.acq_res))
        self.blur_k_lo = self.blur_k_mid - k_radius
        self.blur_k_hi = self.blur_k_mid + k_radius
        
        # Each angle will have a random k and blur_k
        
        ks = [np.random.uniform(self.k_lo, self.k_hi) for _ in angles]
        blur_ks = [np.random.uniform(self.blur_k_lo, self.blur_k_hi) for _ in angles] 

        self.k = self.k_mid
        self.blur_k = self.blur_k_mid

        # Fpaths
        self.img_fpath = img_fpath
        self.lbl_fpath = lbl_fpath

        # Load img and lbl into RAM
        img = nii_obj.get_fdata(dtype=np.float32)
        lbl = lbl_obj.get_fdata(dtype=np.float32)

        # Relabel lbl
        self.label_map = index_labels(lbl)
        self.n_lbl_classes = len(self.label_map)
        lbl = apply_relabel(lbl, self.label_map)

        if img.shape != lbl.shape:
            print("Shape mismatch between image and lbl, exiting...")
            print("Image: {}\nMask: {}".format(img.shape, lbl.shape))
            sys.exit()
            
        if self.train:
            # Get class weights inversely proportional to representation
            n_non_bg_voxels = len(np.where(lbl != 0)[0])

            # Get volume for each label
            class_occurrences = {}
            for l in np.unique(lbl):
                class_occurrences[l] = len(np.where(lbl == l)[0])
                
            # Set class weights as well
            self.class_weights = []
            for k, v in class_occurrences.items():
                self.class_weights.append(v)
            
            # normalize class_weights
            self.class_weights = [np.log(np.max(self.class_weights) /x) for x in self.class_weights]
            self.class_weights = np.array(self.class_weights)
#             self.class_weights = [np.max(self.class_weights) / x for x in self.class_weights]
# #             self.class_weights = [x / np.sum(self.class_weights) for x in self.class_weights]
#             self.class_weights = np.array(self.class_weights)
#             # scale down to 1, then up to 10
#             self.class_weights = self.class_weights / self.class_weights.max() * 100
            # clamp bottom to 1
            self.class_weights[self.class_weights < 1] = 1
            print("Using class weights:", self.class_weights)
                
            # Create a list of labels with inverse proportions
            # skip background with `volumes[1:]`
            self.label_set = []
            
            # we will use this to match the number of non-label coords
            biggest_repr = 0 
            
            for l, v in class_occurrences.items():
                if l == 0:
                    continue
                rep = (n_non_bg_voxels // v)
                self.label_set.extend([l] * rep)
                if rep >= biggest_repr:
                    biggest_repr = rep

            # we also need to add representation for non-bg, non-labels
            # here `-1` will be a flag to switch to a bg-lbl but non-zero img index
            self.label_set.extend([-1] * biggest_repr)

            # Store set of background coords
            bg_coords = set([(x, y, z) for x, y, z in zip(*np.where(lbl == 0))])
        

        ########## PROCESS IMAGE ##########
        print("\n{a} PROCESSING IMAGE {a}".format(a="="*20))

        ##### 1. Orient s.t. z is the LR axis #####
        self.lr_axis = np.argmax(self.acq_res)
        if self.lr_axis == 0:
            img = img.transpose(1, 2, 0)
        elif self.lr_axis == 1:
            img = img.transpose(2, 0, 1)
        # if 2, no change

        ##### 2. Pad to square in-plane #####
        in_plane_shape = max(
            img.shape[0],
            img.shape[1],
        )
        target_shape = (in_plane_shape, in_plane_shape, img.shape[2])
        img, self.pads = target_pad(img, target_shape)

        ##### 3. Add channel dimension #####
        img = img[..., np.newaxis]

        ##### 4. Normalize intensities #####
        print("{: <30}".format("Normalizing..."), end='')
        st = time.time()
        img, *self.inv_normalize_params = self.normalize(img, a=0, b=1)
        self.normalized_mean = img.mean()
        en = time.time()
        print("{:.4f}s".format(en-st))

        if self.train:
            ##### 4. Rotate, blur, alias #####
            print("\nProcessing for each of {} rotations...\n".format(n_rots))
            st = time.time()
            self.imgs_rot = []
            self.imgs_blur = []
            self.imgs_alias = []
            self.imgs_idx = []

            for angle, k, blur_k in zip(angles, ks, blur_ks):
                
                img_rot, img_blur, img_alias = self.process_img(img, angle, k, blur_k)
                self.imgs_rot.append(img_rot)
                self.imgs_blur.append(img_blur)
                self.imgs_alias.append(img_alias)
                
                self.imgs_idx.append(np.array(np.where(img_rot > np.quantile(img_rot, bg_tolerance))))

#                 # Track non-bg locations by `bg_tolerance` percentile                
#                 img_coords = set([(x, y, z) for x, y, z in zip(*np.where(img_rot > np.quantile(img_rot, bg_tolerance)))])
#                 # Intersect with non-label coords
#                 intersection_coords = bg_coords.intersection(img_coords)
#                 # reformat as npindex. TODO: make generic
#                 img_coords = [[],[],[]]
#                 for x, y, z in intersection_coords:
#                     img_coords[0].append(x)
#                     img_coords[1].append(y)
#                     img_coords[2].append(z)
                    
#                 img_coords[0] = np.array(img_coords[0])
#                 img_coords[1] = np.array(img_coords[1])
#                 img_coords[2] = np.array(img_coords[2])
#                 img_coords = np.array(img_coords)
#                 # Append this rotation's coordinates
#                 self.imgs_idx.append(img_coords)

                clear_memory()

            en = time.time()
            print("\tElapsed time to get process image: {:.4f}s".format(en-st))
        else:
            print("\nProcessing for each of {} rotations...\n".format(n_rots))
            st = time.time()
            self.imgs_rot = []

            for angle in angles:
                self.imgs_rot.append(
                    self.process_img(img, angle, degrade=False))
                clear_memory()

            en = time.time()
            print("\tElapsed time to get process image: {:.4f}s".format(en-st))

        ########## PROCESS LBL ##########
        print("\n{a} PROCESSING LBL {a}".format(a="="*20))
        ##### 1. Orient s.t. z is the LR axis #####
        self.lr_axis = np.argmax(self.acq_res)
        if self.lr_axis == 0:
            lbl = lbl.transpose(1, 2, 0)
        elif self.lr_axis == 1:
            lbl = lbl.transpose(2, 0, 1)
        # if 2, no change

        ##### 2. Pad to square in-plane #####
        in_plane_shape = max(
            lbl.shape[0],
            lbl.shape[1],
        )
        target_shape = (in_plane_shape, in_plane_shape, lbl.shape[2])
        lbl, self.pads = target_pad(lbl, target_shape)

        if self.train:
            ##### 3. Membership, rotate, alias, upsample #####
            print("\nProcessing for each of {} rotations...\n".format(n_rots))
            st = time.time()
            self.lbls_rot = []
            self.lbls_blur = []
            self.lbls_alias = []

            # Track lbl indices for all labels
            # This will be a list of dicts
            self.lbls_idx = []

            for angle, k, blur_k in zip(angles, ks, blur_ks):
                lbl_rot, lbl_blur, lbl_alias = self.process_lbl(lbl, angle, k, blur_k)
                self.lbls_rot.append(lbl_rot)
                self.lbls_blur.append(lbl_blur)
                self.lbls_alias.append(lbl_alias)
                self.lbls_idx.append(
                    {v: np.array(np.where(lbl_rot == v))
                     for v in np.unique(lbl_rot)}
                )
                clear_memory()

            en = time.time()
            print("\tElapsed time to process lbl: {:.4f}s".format(en-st))
        else:
            ##### 3. Rotate, upsample #####
            print("\nProcessing for each of {} rotations...\n".format(n_rots))
            st = time.time()
            self.lbls_rot = []

            for angle in angles:
                self.lbls_rot.append(
                    self.process_lbl(lbl, angle, degrade=False))
                clear_memory()

            en = time.time()
            print("\tElapsed time to process lbl: {:.4f}s".format(en-st))

    def __len__(self):
        return self.n_steps

    def set_mode(self, mode):
        self.mode = mode

    def __getitem__(self, i):
        '''
        To be used with DataLoader during training
        '''

        def safe_slice(st, en, p, dim):
            '''
            Avoid out-of-bounds
            '''
            if st < 0:
                st += p
                en += p
            elif en >= dim:
                st -= p
                en -= p

            return slice(st, en)

        # Choose a random rotation
        rot_i = np.random.randint(0, len(self.imgs_rot))

        # select patch by uniformly sampling a label
        # TODO: Sample weights inverse by label representation
        v = np.random.choice(self.label_set)
        if v != -1:
            selection = np.random.randint(0, self.lbls_idx[rot_i][v].shape[1])
            target_idx = self.lbls_idx[rot_i][v][..., selection]
        else:
            selection = np.random.randint(0, self.imgs_idx[rot_i].shape[1])
            target_idx = self.imgs_idx[rot_i][..., selection]

        idx = tuple(
            safe_slice(m-(p//2), m+(p//2), p, s)
            if i < len(self.lbls_rot[0].shape) - 1 else slice(m, m+1)
            for i, (m, p, s) in enumerate(
                zip(target_idx, self.patch_size, self.lbls_rot[0].shape)
            )
        )

        # Both modes expect alias as input
        img_patch_alias = self.imgs_alias[rot_i][idx]
        lbl_patch_alias = self.lbls_alias[rot_i][idx]
        # label to membership
        lbl_patch_alias = lbl_to_membership(lbl_patch_alias.squeeze(), self.n_lbl_classes)
        # channels first
        img_patch_alias = img_patch_alias.transpose(2, 0, 1)
        lbl_patch_alias = lbl_patch_alias.transpose(2, 0, 1)

        if self.mode == 'SR':  # SR learns alias -> HR
            img_patch = self.imgs_rot[rot_i][idx]
            
            lbl_patch = self.lbls_rot[rot_i][idx]
            lbl_patch = lbl_to_membership(lbl_patch.squeeze(), self.n_lbl_classes)
            
            img_patch = img_patch.transpose(2, 0, 1)
            lbl_patch = lbl_patch.transpose(2, 0, 1)
            return img_patch_alias, lbl_patch_alias, img_patch, lbl_patch

        # Note: for AA mode, lbl_blur is the same as `lbl`.
        # This means that the lbl branch in AA mode is learning the same problem as SR mode.

        # AA mode learns alias -> blur
        elif self.mode == 'AA':  # AA learns alias -> blur;
            img_patch_blur = self.imgs_blur[rot_i][idx]
            
            lbl_patch_blur = self.lbls_blur[rot_i][idx]
            lbl_patch_blur = lbl_to_membership(lbl_patch_blur.squeeze(), self.n_lbl_classes)
            
            img_patch_blur = img_patch_blur.transpose(2, 0, 1)
            lbl_patch_blur = lbl_patch_blur.transpose(2, 0, 1)
            return img_patch_alias, lbl_patch_alias, img_patch_blur, lbl_patch_blur

    def process_img(self, img, angle, k=1, blur_k=1, degrade=True):
        '''

        If degrade is False, only rotate + upsample.
        Otherwise:

        Process image by:
            1. applying rotation
            2. blurring (1)
            3. aliasing (2)
            4. spline interpolate (1, 2, 3)
            5. return these three volumes
        '''
        print("{: <30}".format("Rotating at {} degrees...".format(angle)), end='')
        st = time.time()
        img_rot = rotate_vol_2D(img, angle)
        en = time.time()
        print("{:.4f}s".format(en-st))

        if degrade:
            print("{: <30}".format("Degrading..."), end='')
            st = time.time()
            img_blur = blur(img_rot, blur_k=blur_k, axis=0)
            img_alias = alias(img_blur, k=k,
                              down_order=1, up_order=1, axis=0)
            en = time.time()
            print("{:.4f}s".format(en-st))

#             print("{: <30}".format("Interpolating..."), end='')
#             st = time.time()
# #             img_rot = spline_interp_old(img_rot, k=self.k, axis=2)
# #             img_blur = spline_interp_old(img_blur, k=self.k, axis=2)
# #             img_alias = spline_interp_old(img_alias, k=self.k, axis=2)
#             img_rot = spline_interp(img_rot, k=k, batch_size=1)
#             img_blur = spline_interp(img_blur, k=k, batch_size=1)
#             img_alias = spline_interp(img_alias, k=k, batch_size=1)
#             en = time.time()
#             print("{:.4f}s".format(en-st))

            # Discard channel dimension
            img_rot = img_rot[..., 0]
            img_blur = img_blur[..., 0]
            img_alias = img_alias[..., 0]

            return img_rot, img_blur, img_alias
        else:
            print("{: <30}".format("Interpolating..."), end='')
            st = time.time()
#             img_rot = spline_interp_old(img_rot, k=self.k, axis=2)
            img_rot = spline_interp(img_rot, k=self.k, batch_size=1)
            en = time.time()
            print("{:.4f}s".format(en-st))

            # Discard channel dimension
            img_rot = img_rot[..., 0]

            return img_rot

    def process_lbl(self, lbl, angle, k=1, blur_k=1, degrade=True):
        '''
        If degrade is False, only rotate + upsample.
        Otherwise:
        Process lbl by:
            1. lbl -> membership
            2. applying rotation
            3. aliasing (2)
            4. spline interpolate (2, 3)
            5. membership -> lbl (4)
            5. return these two volumes
        '''
        print("{: <30}".format("Mask -> membership..."), end='')
        st = time.time()
        memb = lbl_to_membership(lbl, self.n_lbl_classes)
        en = time.time()
        print("{:.4f}s".format(en-st))

        print("{: <30}".format("Rotating at {} degrees...".format(angle)), end='')
        st = time.time()
        memb_rot = rotate_vol_2D(memb, angle)
        en = time.time()
        print("{:.4f}s".format(en-st))

        if degrade:
            print("{: <30}".format("Degrading..."), end='')
            st = time.time()
            memb_blur = blur(memb_rot, blur_k=blur_k, axis=0)
            memb_alias = alias(memb_blur, k=k,
                               down_order=0, up_order=0, axis=0)
            en = time.time()
            print("{:.4f}s".format(en-st))

#             print("{: <30}".format("Interpolating..."), end='')
#             st = time.time()
#             memb_rot = spline_interp(memb_rot, k=k, batch_size=8)
#             memb_blur = spline_interp(memb_blur, k=k, batch_size=8)
#             memb_alias = spline_interp(memb_alias, k=k, batch_size=8)
#             en = time.time()
#             print("{:.4f}s".format(en-st))

            print("{: <30}".format("Memberships -> lbls..."), end='')
            st = time.time()
            lbl_rot = membership_to_lbl(memb_rot)
            lbl_blur = membership_to_lbl(memb_blur)
            lbl_alias = membership_to_lbl(memb_alias)
            en = time.time()
            print("{:.4f}s".format(en-st))

            return lbl_rot, lbl_blur, lbl_alias
        else:
            print("{: <30}".format("Interpolating..."), end='')
            st = time.time()
            memb_rot = spline_interp(memb_rot, k=self.k, batch_size=8)
            en = time.time()
            print("{:.4f}s".format(en-st))

            print("{: <30}".format("Memberships -> lbls..."), end='')
            st = time.time()
            lbl_rot = membership_to_lbl(memb_rot)
            en = time.time()
            print("{:.4f}s".format(en-st))

            return lbl_rot

    def get_vols(self):
        '''
        During test-time, return the list of rotated images.
        Pad each of them to mitigate edge effects
        '''
        buf = 2 * self.kernel_size
        target_dims = (
            self.imgs_rot[0].shape[0] + buf,
            self.imgs_rot[0].shape[1] + buf,
            self.imgs_rot[0].shape[2] + buf,
        )

        imgs = []
        for img in self.imgs_rot:
            img_pad, bufs = target_pad(img, target_dims)
            self.bufs = bufs
            imgs.append(img_pad)

        lbls = []
        for lbl in self.lbls_rot:
            lbl_pad, bufs = target_pad(lbl, target_dims)
            self.bufs = bufs
            lbl_pad = lbl_to_membership(lbl_pad, self.n_lbl_classes)
            lbls.append(lbl_pad)

        return imgs, lbls

    def format_pads(self, pads):
        '''
        Turn pad amounts into appropriate slices
        and handle 0 pads as None slices
        '''
        st = pads[0] if pads[0] != 0 else None
        en = -pads[1] if pads[1] != 0 else None
        return slice(st, en)

    def crop(self, img, pads):
        ''' 
        remove buffer added in `get_vols()`

        Always nonzero so no issue
        '''
        crops = tuple(map(self.format_pads, pads))
        return img[crops]

    def reorient(self, img):
        '''
        Orient s.t. axes correspond with original axes
        '''

        if self.lr_axis == 0:
            return img.transpose(2, 0, 1)
        elif self.lr_axis == 1:
            return img.transpose(1, 2, 0)
        elif self.lr_axis == 2:
            return img

    def invmap_labels(self, lbl):
        # Relabel lbl
        inv_label_map = {v: k for k, v in self.label_map.items()}
        return apply_relabel(lbl, inv_label_map)

    def normalize(self, x, a=-1, b=1):
        orig_mean = x.mean()
        orig_std = x.std()
        x = (x - orig_mean) / orig_std
        
        return x, orig_mean, orig_std
#         squash_min = x.min()
#         squash_max = x.max()
#         x = a + ((x - squash_min)*(b-a)) / (squash_max - squash_min)
#         return x, squash_min, squash_max, orig_mean, orig_std, a, b

    def inv_normalize(self, x):
        orig_mean, orig_std = self.inv_normalize_params
        tmp = x * orig_std
        tmp += orig_mean
        return tmp
        
#         squash_min, squash_max, orig_mean, orig_std, a, b = self.inv_normalize_params

#         tmp = x - a
#         tmp *= squash_max - squash_min
#         tmp /= b - a
#         tmp += squash_min
#         tmp *= orig_std
#         tmp += orig_mean

#         return tmp

    def write_nii(self, x, out_fpath):
        '''
        Write x as a nifti object.
        Uses the image's affine and header. This should be the same
        as the lbl's; if not, there's an issue in the lbl creation (outside of
        this program).
        '''
        # Update affine for super-resolved version
        new_scales = [1, 1, 1]
        new_scales[self.lr_axis] = 1/self.k
        new_scales = tuple(new_scales)
        new_affine = np.matmul(self.img_affine, np.diag(new_scales + (1,)))
        out_obj = nib.Nifti1Image(
            x,
            affine=new_affine,
            header=self.img_header,
        )
        nib.save(out_obj, out_fpath)
