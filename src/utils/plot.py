import matplotlib.pyplot as plt
import numpy as np
import time
from IPython import display
from PIL import Image

plt.rcParams['figure.facecolor'] = 'black'
plt.rcParams['figure.figsize'] = (25, 25)
plt.rcParams['image.cmap'] = 'Greys_r'
plt.rcParams['image.interpolation'] = 'nearest'

def multihist(imgs, bins=None, titles=None):
    fig, axs = plt.subplots(1, len(imgs), facecolor='white', figsize=(8*len(imgs), 4))

    if titles is None:
        titles = list(range(len(imgs)))
        
    # Fix x-lim    
    xlim_min = min([np.min(img) for img in imgs]) - 50
    xlim_max = max([np.max(img) for img in imgs])
    
    # Choose bins
    if bins is None:
        bins = 1024
    
    for ax, img, title in zip(axs, imgs, titles):
        ax.hist(img.flatten(), bins=bins, color='darkred', lw=0)
        ax.set_yscale('log')
        ax.set_xlim((xlim_min, xlim_max))
        ax.set_title(title)
        # Hide the spines
#         ax.spines['right'].set_visible(False)
#         ax.spines['left'].set_visible(False)
#         ax.spines['top'].set_visible(False)
#         ax.spines['bottom'].set_visible(False)

    plt.show()

def multiplot(imgs, titles, vmin=None, vmax=None, cmap='Greys_r'):
    fig, axs = plt.subplots(1, len(imgs))

    if vmin is None:
        vmin = [None] * len(imgs)
    if vmax is None:
        vmax = [None] * len(imgs)
    for ax, img, title, cur_vmin, cur_vmax in zip(axs, imgs, titles, vmin, vmax):
        if cur_vmin is None:
            cur_vmin = img.min()
        if cur_vmax is None:
            cur_vmax = img.max()
        ax.imshow(np.rot90(img), vmin=cur_vmin, vmax=cur_vmax, cmap=cmap)
        ax.set_title(title)
        ax.set_xticks([])
        ax.set_yticks([])
        # Hide the spines
        ax.spines['right'].set_visible(False)
        ax.spines['left'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.spines['bottom'].set_visible(False)

    plt.show()
    
def center_vol_plot(img_vol, target_shape=None, cs=None, vmin=None, vmax=None, cmap='Greys_r'):
    
    if cs is None:
        cs = [c//2 for c in img_vol.shape]
    
    x = img_vol[cs[0], :, :]
    y = img_vol[:, cs[1], :]
    z = img_vol[:, :, cs[2]]
    
    
    # For anisotropic images, provide `target_shape` for NN interp
    if target_shape is not None:
        # check slice shapes
        if x.shape != (target_shape[1], target_shape[2]):
            # PIL resize needs (y, x)
            x = np.array(Image.fromarray(x)\
                         .resize((target_shape[2], target_shape[1]), 
                                 Image.NEAREST))
        if y.shape != (target_shape[0], target_shape[2]):
            y = np.array(Image.fromarray(y)\
                         .resize((target_shape[2], target_shape[0]), 
                                 Image.NEAREST))
        if z.shape != (target_shape[0], target_shape[1]):
            z = np.array(Image.fromarray(z)\
                         .resize((target_shape[1], target_shape[0]), 
                                 Image.NEAREST))
   
    multiplot(
        [x, y, z],
        [1,2,3],
        vmin=vmin,
        vmax=vmax,
        cmap=cmap,
    )
    
def idx_vol_plot(img_vol, idx, target_shape=None, vmin=None, vmax=None):
    
    x = img_vol[idx[0], :, :]
    y = img_vol[:, idx[1], :]
    z = img_vol[:, :, idx[2]]
    
    
    # For anisotropic images, provide `target_shape` for NN interp
    if target_shape is not None:
        # check slice shapes
        if x.shape != (target_shape[1], target_shape[2]):
            # PIL resize needs (y, x)
            x = np.array(Image.fromarray(x)\
                         .resize((target_shape[2], target_shape[1]), 
                                 Image.NEAREST))
        if y.shape != (target_shape[0], target_shape[2]):
            y = np.array(Image.fromarray(y)\
                         .resize((target_shape[2], target_shape[0]), 
                                 Image.NEAREST))
        if z.shape != (target_shape[0], target_shape[1]):
            z = np.array(Image.fromarray(z)\
                         .resize((target_shape[1], target_shape[0]), 
                                 Image.NEAREST))
   
    multiplot(
        [x, y, z],
        [1,2,3],
        vmin=[vmin]*3,
        vmax=[vmax]*3,
    )
    
def anim_paired_patches(lr_patch, hr_patch):
    fig, axs = plt.subplots(1, 2)
    
    vmin = lr_patch.min()
    vmax = lr_patch.max()
    
    axs[0].imshow(np.rot90(lr_patch), vmin=vmin, vmax=vmax)
    axs[1].imshow(np.rot90(hr_patch), vmin=vmin, vmax=vmax)
    
    for ax in axs:
        ax.set_xticks([])
        ax.set_yticks([])

    display.display(plt.show())
    display.clear_output(wait=True)
    time.sleep(.1)