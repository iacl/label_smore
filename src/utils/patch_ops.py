import numpy as np
import torch
from sklearn.utils import shuffle


def get_training_pair(
        views_list_x,
        views_list_y,
        index_iter,
        patch_size,
        batch_size,
        n_layers,
        kernel_size,
        mean_thresh,
        seed,
):
    c = patch_size[-1]
    h = patch_size[0]
    w = patch_size[1]

    xs = np.zeros((batch_size, c, h, w), dtype=np.float32)
    ys = np.zeros((batch_size, c, h, w), dtype=np.float32)

    idx_order = list(range(batch_size))
    idx_order = shuffle(idx_order, random_state=seed)

    insertion_idx = 0

    while insertion_idx < batch_size:

        i = next(index_iter)
        # select a random rotation from the list
        rot_idx = np.random.choice(len(views_list_x))

        if views_list_y[rot_idx][i].mean() < mean_thresh:
            continue

        # Take copies for the batch to allow augmentation
        x = views_list_x[rot_idx][i].copy()
        y = views_list_y[rot_idx][i].copy()

        # random flip along X-axis
        if np.random.choice([True, False]):
            x = x[::-1, ...]
            y = y[::-1, ...]

        # random flip along Y-axis
        if np.random.choice([True, False]):
            x = x[:, ::-1, ...]
            y = y[:, ::-1, ...]

        for cur_c in range(c):
            xs[idx_order[insertion_idx], cur_c] = x[..., cur_c]
            ys[idx_order[insertion_idx], cur_c] = y[..., cur_c]

        insertion_idx += 1

    xs = torch.from_numpy(xs).float().cuda()
    ys = torch.from_numpy(ys).float().cuda()

    return xs, ys
