import nibabel as nib
import numpy as np
import time
import sys
import os
import argparse

from tqdm import tqdm
from pathlib import Path
from scipy import ndimage

from utils.membership import *


def spline_interp(x, k, order=3, axis=2):
    down_scale = tuple(
        k if i == axis else 1 for i in range(len(x.shape))
    )
    return ndimage.zoom(x, down_scale, order=order)


def get_distance(f):
    """Return the signed distance to the 0.5 levelset of a function."""
    ''' code from
    https://github.com/pmneila/morphsnakes/issues/5#issuecomment-203014643
    '''

    # Prepare the embedding function.
    f = f > 0.5

    # Signed distance transform
    dist_func = ndimage.distance_transform_edt
    distance = np.where(f, dist_func(f) - 0.5, -(dist_func(1-f) - 0.5))

    return distance


def nearest_label(label_val, label_set):
    '''
    Find the closest label to the class label

    Like rounding but within possible classes
    '''
    nearest_label = label_set[0]
    closest_dist = np.abs(label_val - nearest_label)

    for label in label_set:
        cur_dist = np.abs(label_val - label)
        if cur_dist < closest_dist:
            nearest_label = label
            closest_dist = cur_dist

    return nearest_label


def to_label_set(mask, label_set):
    '''
    Return the mask to label set
    '''
    d = {v: nearest_label(v, label_set) for v in np.unique(mask)}

    for v in np.unique(mask):
        mask[np.where(mask == v)] = d[v]
    return mask


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument("--infile", type=str)
    parser.add_argument("--is_mask", type=str)
    parser.add_argument("--outdir", type=str)
    parser.add_argument("--order", type=int)
    parser.add_argument("--k", type=float)
    parser.add_argument("--lr_axis", type=int)

    args = parser.parse_args()

    IN_FPATH = Path(args.infile)
    OUT_DIR = Path(args.outdir)
    OUT_FPATH = OUT_DIR / IN_FPATH.name

    is_mask = True if args.is_mask.lower() == "true" else False
    k = args.k
    lr_axis = args.lr_axis

    ########## PROCESSING ##########

    in_obj = nib.load(IN_FPATH)
    in_img = in_obj.get_fdata()

    new_scales = [1, 1, 1]
    new_scales[lr_axis] = 1 / k
    new_scales = tuple(new_scales)
    new_affine = np.matmul(in_obj.affine, np.diag(new_scales + (1,)))

    out_shape = tuple(s if i != lr_axis else int(s * k)
                      for (i, s) in enumerate(in_img.shape))

    # actually do the interpolation
    if is_mask:
        # Relabel mask
        label_map = index_labels(in_img)
        n_labels = len(label_map)
        in_img_relabel = apply_relabel(in_img, label_map)

        # Convert to membership
        in_mem = mask_to_membership(in_img_relabel, n_labels)

        # Interpolate each class independently
        mem_interp = np.zeros((*out_shape, n_labels))

        for c in range(n_labels):

            mem_interp[..., c] = spline_interp(in_mem[..., c], k,
                                               order=args.order, axis=lr_axis)
        out_img = membership_to_mask(mem_interp)

        # Relabel
        inv_label_map = {v: k for k, v in label_map.items()}
        out_img = apply_relabel(out_img, inv_label_map)
    else:
        out_img = spline_interp(in_img, k, order=args.order, axis=lr_axis)

    out_obj = nib.Nifti1Image(
        out_img, affine=new_affine, header=in_obj.header)
    nib.save(out_obj, OUT_FPATH)
