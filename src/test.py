import argparse
import nibabel as nib
import numpy as np
import torch
import time
import sys
import os

from models.edsr import *
from models.losses import *

from utils.img_and_lbl_loader import *
from utils.fba import *

from tqdm import tqdm
from pathlib import Path

np.random.seed(0)


def apply_model_volume_slices(img_vol, lbl_vol, model):

    out_img_vol = np.zeros_like(img_vol, dtype=np.float32)
    out_lbl_vol = np.zeros_like(img_vol, dtype=np.float32)

    i = 0

    with tqdm(total=len(img_vol)) as pbar:
        
        for i, (img_slice, lbl_slice) in enumerate(zip(img_vol, lbl_vol)):
            # Organize into (batch, channel, h, w)
            img_slice = img_slice[np.newaxis, np.newaxis, ...]
            lbl_slice = lbl_slice[np.newaxis, ...].transpose(0, 3, 1, 2)
            # Convert to tensor, send to GPU
            img_slice = torch.from_numpy(img_slice).cuda()
            lbl_slice = torch.from_numpy(lbl_slice).cuda()
        
            with torch.cuda.amp.autocast():
                img_slice_pred, lbl_slice_pred = model(img_slice, lbl_slice)
                
                # apply softmax to lbl slices along channel dim
                lbl_slice_pred = torch.nn.Softmax(dim=1)(lbl_slice_pred)

                # Back to numpy
                img_slice_pred = img_slice_pred\
                    .detach()\
                    .cpu()\
                    .numpy()\
                    .astype(np.float32)
                lbl_slice_pred = lbl_slice_pred\
                    .detach()\
                    .cpu()\
                    .numpy()\
                    .astype(np.float32)

                # argmax along channel dim
                lbl_slice_pred = np.argmax(lbl_slice_pred, axis=1)
                
            # place in output
            out_img_vol[i] = img_slice_pred.copy()
            out_lbl_vol[i] = lbl_slice_pred.copy()
            
            pbar.update(1)

    out_img_vol = out_img_vol.squeeze()
    out_lbl_vol = out_lbl_vol.squeeze()

    return out_img_vol, out_lbl_vol


def inv_get_n_rots(imgs_xyz, min_angle=0, max_angle=90, n_cores=1):
    # note: actually `n_rots` + 1 rotations are used
    n_rots = len(imgs_xyz) - 1

    if n_rots == 0:
        # no rotations
        return imgs_xyz
    angles = range(min_angle, max_angle+1, max_angle//n_rots)

    # move z axis to front to iterate over xy slices
    imgs_zxy = [img.transpose(2, 0, 1) for img in imgs_xyz]

    # Lambda functions to assist legibility of parallel call
    def slice_rot(sl, angle):
        if angle == 0:
            return sl
        elif angle == -90:
            return np.rot90(sl, k=-1)
        elif angle == -180:
            return np.rot90(sl, k=-2)
        elif angle == -270:
            return np.rot90(sl, k=-3)
        print("doing real rot")
        return rotate(sl, angle, reshape=False, mode='reflect')

    def apply_rot_to_vol(vol, angle): return np.array(
        [slice_rot(sl, -angle) for sl in vol]
    ).transpose(1, 2, 0)

    imgs_xyz = Parallel(n_jobs=n_cores, prefer="threads")(
        delayed(apply_rot_to_vol)(img_zxy, angle)
        for img_zxy, angle in zip(imgs_zxy, angles))

    return imgs_xyz


if __name__ == '__main__':

    main_st = time.time()
    print("\n{} BEGIN PREDICTION {}\n".format("="*20, "="*20))
    #################### ARGUMENTS ####################

    parser = argparse.ArgumentParser()
    parser.add_argument("--infile", type=str)
    parser.add_argument("--inlbl", type=str)
    parser.add_argument("--outfile", type=str)
    parser.add_argument("--outlbl", type=str)
    parser.add_argument("--weight_dir", type=str)
    parser.add_argument("--gpu_id", type=str)
    parser.add_argument("--patch_size", type=int)
    parser.add_argument("--n_resblocks", type=int)
    parser.add_argument("--n_rots", type=int)
    parser.add_argument("--filters", type=int)
    parser.add_argument("--kernel_size", type=int)
    parser.add_argument("--n_cores", type=int)
    parser.add_argument("--acq_dim", type=int)
    parser.add_argument("--lr_axis", type=str, default=None)
    parser.add_argument("--k", type=float, default=None)
    parser.add_argument("--img_p", type=str, default="inf")
    parser.add_argument("--lbl_p", type=str, default="0")
    parser.add_argument("--clamp_floor", type=str, default=None)
    parser.add_argument("--clamp_ceil", type=str, default=None)

    args = parser.parse_args()

    filters = args.filters
    n_resblocks = args.n_resblocks
    n_rots = args.n_rots
    kernel_size = args.kernel_size
    N_CORES = args.n_cores
    ACQ_DIM = args.acq_dim
    lr_axis = args.lr_axis
    k = args.k
    img_p = args.img_p
    lbl_p = args.lbl_p
    if not img_p == "inf" and not img_p == "infinity":
        img_p = int(img_p)
    if not lbl_p == "inf" and not lbl_p == "infinity":
        lbl_p = int(lbl_p)
            
    PATCH_SIZE = (args.patch_size, args.patch_size, 1)

    IN_FPATH = Path(args.infile)
    IN_LBL_FPATH = Path(args.inlbl)
    OUT_FNAME = Path(args.outfile)
    OUT_LBL_FNAME = Path(args.outlbl)
    OUT_DIR = OUT_FNAME.parent
    WEIGHT_DIR = Path(args.weight_dir)

    GPU_ID = args.gpu_id
    os.environ['CUDA_VISIBLE_DEVICES'] = GPU_ID

    if not OUT_FNAME.parent.exists():
        OUT_FNAME.parent.mkdir(parents=True)

    #################### LOAD AND PROCESS DATA ####################

    dataset = ImageAndMask(
        img_fpath=IN_FPATH,
        lbl_fpath=IN_LBL_FPATH,
        patch_size=PATCH_SIZE,
        n_resblocks=n_resblocks,
        n_steps=None,
        n_rots=n_rots,
        kernel_size=kernel_size,
        train=False,
    )

    n_lbl_classes = dataset.n_lbl_classes

    imgs_xyz, lbls_xyz = dataset.get_vols()
    imgs_out = []
    lbls_out = []

    #################### MODEL SETUP ####################

    WEIGHT_FNAME = "{}_best_weights.h5"

    AA_WEIGHT_PATH = WEIGHT_DIR / WEIGHT_FNAME.format('AA')
    aa_checkpoint = torch.load(AA_WEIGHT_PATH)
    aa_model = EDSR(
        n_img_channels=1,
        n_lbl_classes=n_lbl_classes,
        kernel_size=kernel_size,
        n_resblocks=n_resblocks,
        filters=filters,
        padding_mode='zeros',
    ).cuda()
    aa_model.load_state_dict(aa_checkpoint['model'])

    SR_WEIGHT_PATH = WEIGHT_DIR / WEIGHT_FNAME.format('SR')
    sr_checkpoint = torch.load(SR_WEIGHT_PATH)
    sr_model = EDSR(
        n_img_channels=1,
        n_lbl_classes=n_lbl_classes,
        kernel_size=kernel_size,
        n_resblocks=n_resblocks,
        filters=filters,
        padding_mode='zeros',
    ).cuda()
    sr_model.load_state_dict(sr_checkpoint['model'])

    #################### PREDICT ####################

    st = time.time()

    for img_xyz, lbl_xyz in zip(imgs_xyz, lbls_xyz):
        
        # AA
        img_yzx = img_xyz.transpose(1, 2, 0)
        lbl_yzx = lbl_xyz.transpose(1, 2, 0, 3)
        
        print(img_xyz.min(), img_xyz.mean(), img_xyz.max())
        
        print("Applying AA network...")
        img_aa_yzx, lbl_aa_yzx = apply_model_volume_slices(
            img_vol=img_yzx,
            lbl_vol=lbl_yzx,
            model=aa_model,
        )

        # transpose back
        img_aa_xyz = img_aa_yzx.transpose(2, 0, 1)
        lbl_aa_xyz = lbl_aa_yzx.transpose(2, 0, 1)
        lbl_aa_xyz = lbl_to_membership(lbl_aa_xyz, dataset.n_lbl_classes)
#         # Zero-out negative values
#         img_aa_xyz[np.where(img_aa_xyz < 0)] = 0

        # Zero mean, unit variance
        img_aa_xyz = img_aa_xyz - img_aa_xyz.mean()
        img_aa_xyz = img_aa_xyz / img_aa_xyz.std()
        
        print(img_aa_xyz.min(), img_aa_xyz.mean(), img_aa_xyz.max())
        
        # SR
        img_aa_xzy = img_aa_xyz.transpose(0, 2, 1)
        lbl_aa_xzy = lbl_aa_xyz.transpose(0, 2, 1, 3)

        print("Applying SR network...")
        img_sr_xzy, lbl_sr_xzy = apply_model_volume_slices(
            img_vol=img_aa_xzy,
            lbl_vol=lbl_aa_xzy,
            model=sr_model,
        )

        # transpose back
        img_sr_xyz = img_sr_xzy.transpose(0, 2, 1)
        lbl_sr_xyz = lbl_sr_xzy.transpose(0, 2, 1)
#         # Zero-out negative values
#         img_sr_xyz[np.where(img_sr_xyz < 0)] = 0
        # Zero mean, unit variance
        img_sr_xyz = img_sr_xyz - img_sr_xyz.mean()
        img_sr_xyz = img_sr_xyz / img_sr_xyz.std()

        print(img_sr_xyz.min(), img_sr_xyz.mean(), img_sr_xyz.max())        

        imgs_out.append(img_sr_xyz)
        lbls_out.append(lbl_sr_xyz)

    en = time.time()
    print("Time for prediction : {:.4f}s".format(en-st))

    ########## Return to image space ##########

    print("Returning to image space...")
    imgs_out = inv_get_n_rots(imgs_out, max_angle=90, n_cores=N_CORES)
    imgs_out = [dataset.crop(img, dataset.bufs) for img in imgs_out]
    imgs_out = [dataset.crop(img, dataset.pads) for img in imgs_out]
    imgs_out = [dataset.reorient(img) for img in imgs_out]
    img_out = fba(imgs_out, p=img_p, n_cores=N_CORES)
    print(img_out.min(), img_out.mean(), img_out.max())
    
    
    # inv normalize
    print("Returning to original intensity space...")
    print(img_out.min(), img_out.mean(), img_out.max())
#     img_out = img_out / img_out.max()
    img_out = dataset.inv_normalize(img_out)
    print(img_out.min(), img_out.mean(), img_out.max())
    if args.clamp_floor is not None:
        img_out[img_out < float(args.clamp_floor)] = float(args.clamp_floor)
    if args.clamp_ceil is not None:
        img_out[img_out > float(args.clamp_ceil)] = float(args.clamp_ceil)
    print(img_out.min(), img_out.mean(), img_out.max())
    
    ########## Return to lbl space ##########

    print("Returning to lbl space...")
    lbls_out = inv_get_n_rots(lbls_out, max_angle=90, n_cores=N_CORES)
    lbls_out = [dataset.crop(lbl, dataset.bufs) for lbl in lbls_out]
    lbls_out = [dataset.crop(lbl, dataset.pads) for lbl in lbls_out]
    lbls_out = [dataset.reorient(lbl) for lbl in lbls_out]
    # membership before FBA
    membs_out = np.zeros(
        (len(lbls_out), *lbls_out[0].shape, n_lbl_classes),
        dtype=np.float32,
    )
    for i, lbl in enumerate(lbls_out):
        memb = lbl_to_membership(lbl, dataset.n_lbl_classes)
        
        for c in tqdm(range(n_lbl_classes)):
            membs_out[i, ..., c] = memb[..., c]
    membs_fba = np.zeros_like(membs_out[0], dtype=np.float32)

    print("Channel-wise FBA...")
    for c in tqdm(range(n_lbl_classes)):
        membs_fba[..., c] = fba(membs_out[..., c], p=lbl_p, n_cores=N_CORES)

    lbl_out = membership_to_lbl(membs_fba)
    lbl_out = lbl_out.astype(np.int32)
    lbl_out = dataset.invmap_labels(lbl_out).astype(np.int32)

    ########## Save images ##########

    print("Saving images...")

    # Save Image
    dataset.write_nii(img_out, OUT_FNAME)
    dataset.write_nii(lbl_out, OUT_LBL_FNAME)

    main_en = time.time()
    print("\n\nDONE\nElapsed time: {:.4f}s\n".format(main_en - main_st))
    print("\tWritten to:\n\t{}\n\t{}".format(OUT_FNAME, OUT_LBL_FNAME))
    print("\n{} END PREDICTION {}\n".format("="*20, "="*20))
