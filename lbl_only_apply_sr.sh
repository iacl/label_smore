#!/bin/bash

GPU_ID=$1
DATASET=$2

APPLY_TRAIN="true"
APPLY_TEST="true"

N_CORES=6
N_ITEMS=500000
FILTERS=256
N_RESBLOCKS=32
KERNEL_SIZE=3
PATCH_SIZE=32
BATCH_SIZE=64
TEST_BATCH_SIZE=1
N_TEST_ROTS=1
LEARNING_RATE="1e-4"
NOISE_LEVEL=0
FBA_LBL_P='0'


echo ${EXPERIMENT}

OUT_DIR="/ISFILE3/USERS/remediossw/data/results/label_smore/OASIS/lbl_only"

if [ ${DATASET} = "ADNI" ] ; then
    IN_RES=$3
    IN_DIR="/ISFILE3/USERS/remediossw/data/ADNI_phantom_1mm_inplane_header_corrected"
    IN_LBL_DIR="/ISFILE3/USERS/remediossw/data/ADNI_phantom_1mm_inplane_header_corrected_masks"
    IN_LBL_FPATH="${IN_LBL_DIR}/SUPERRES-ADNIPHANTOM_20200711_PHANTOM-T2-TSE-2D-CORONAL-PRE-ACQ1-${IN_RES}mm_resampled_mask.nii.gz"
    EXPERIMENT="${IN_RES}_${DATASET}_edsr_resblocks-${N_RESBLOCKS}_patch-${PATCH_SIZE}_filters-${FILTERS}"
fi

if [ ${DATASET} = "shepplogan" ] ; then
    IN_RES=$3
    IN_DIR="/ISFILE3/USERS/remediossw/data/shepp_logan_phantom"
    IN_LBL_DIR="/ISFILE3/USERS/remediossw/data/shepp_logan_phantom_masks"
    IN_LBL_FPATH="${IN_LBL_DIR}/SUPERRES-SHEPPLOGANPHANTOM_20201029_PHANTOM-PHANTOM-PHANTOM-2D-SAGITTAL-PRE-ACQ1-${IN_RES}mm.nii.gz"
    EXPERIMENT="${IN_RES}_${DATASET}_edsr_resblocks-${N_RESBLOCKS}_patch-${PATCH_SIZE}_filters-${FILTERS}"
fi

if [ ${DATASET} = "IXI" ] ; then
    IN_LBL_FPATH=$3
    EXPERIMENT="${DATASET}_edsr_resblocks-${N_RESBLOCKS}_patch-${PATCH_SIZE}_filters-${FILTERS}"
fi

if [ ${DATASET} = "OASIS" ] ; then
    IN_LBL_FPATH=$3
    EXPERIMENT="${DATASET}_edsr_resblocks-${N_RESBLOCKS}_patch-${PATCH_SIZE}_filters-${FILTERS}"
fi


IN_LBL_FNAME=$(basename ${IN_LBL_FPATH})
OUT_LBL_FPATH="${OUT_DIR}/SR_lbl_${EXPERIMENT}_${IN_LBL_FNAME}"

WEIGHT_DIR="./weights/label_smore/lbl_only/${IN_LBL_FNAME}"
ACQ_DIM="2"

if [ ${APPLY_TRAIN} = "true" ] ; then
    python "src/just_lbl_train.py" \
        --inmask ${IN_LBL_FPATH} \
        --experiment ${EXPERIMENT} \
        --weight_dir ${WEIGHT_DIR} \
        --gpu_id ${GPU_ID} \
        --n_cores ${N_CORES} \
        --n_items ${N_ITEMS} \
        --filters ${FILTERS} \
        --batch_size ${BATCH_SIZE} \
        --patch_size ${PATCH_SIZE} \
        --n_resblocks ${N_RESBLOCKS} \
        --kernel_size ${KERNEL_SIZE} \
        --acq_dim ${ACQ_DIM} \
        --noise_level ${NOISE_LEVEL} \
        --learning_rate ${LEARNING_RATE}
fi

if [ ${APPLY_TEST} = "true" ] ; then
    python "src/just_lbl_test.py" \
        --inmask ${IN_LBL_FPATH} \
        --experiment ${EXPERIMENT} \
        --outmask ${OUT_LBL_FPATH} \
        --weight_dir ${WEIGHT_DIR} \
        --gpu_id ${GPU_ID} \
        --n_cores ${N_CORES} \
        --filters ${FILTERS} \
        --patch_size ${PATCH_SIZE} \
        --batch_size ${TEST_BATCH_SIZE} \
        --n_resblocks ${N_RESBLOCKS} \
        --n_rots ${N_TEST_ROTS} \
        --kernel_size ${KERNEL_SIZE} \
        --acq_dim ${ACQ_DIM} \
        --mask_p ${FBA_LBL_P}
fi
