# Joint Image and Label Self-Super-Resolution

## Installation
If tensorflow is already installed:
 
`pip install -r requirements.txt`

## Running
To run this algorithm, simply call `apply_ssr.sh` and invoke the appropriate arguments.
```
GPU_ID=0
IN_IMG_FPATH=/path/to/mr/image.nii.gz
IN_LBL_FPATH=/path/to/label/volume.nii.gz
OUT_DIR=/path/to/output/directory/

apply_ssr GPU_ID IN_IMG_FPATH IN_LBL_FPATH OUT_DIR
```
