#!/bin/bash

GPU_ID=$1

APPLY_TRAIN="true"
APPLY_TEST="true"

N_CORES=6
N_ITEMS=500000
FILTERS=256
N_RESBLOCKS=8
KERNEL_SIZE=3
PATCH_SIZE=32
BATCH_SIZE=128
N_TRAIN_ROTS=9
N_TEST_ROTS=1
LEARNING_RATE="1e-4"
NOISE_LEVEL=0
FBA_IMG_P='0'
FBA_LBL_P='0'

OUT_DIR="/ISFILE3/USERS/remediossw/data/results/label_smore/OASIS/explicit"

IN_IMG_FPATH=$2
IN_LBL_FPATH=$3
EXPERIMENT="${DATASET}_edsr_resblocks-${N_RESBLOCKS}_patch-${PATCH_SIZE}_filters-${FILTERS}"

IN_IMG_FNAME=$(basename ${IN_IMG_FPATH})
IN_LBL_FNAME=$(basename ${IN_LBL_FPATH})

OUT_IMG_FPATH="${OUT_DIR}/SR_img_${IN_IMG_FNAME}"
OUT_LBL_FPATH="${OUT_DIR}/SR_lbl_${IN_LBL_FNAME}"

WEIGHT_DIR="./weights/label_smore/explicit/${IN_IMG_FNAME}"
ACQ_DIM="2"

if [ ${APPLY_TRAIN} = "true" ] ; then
    python "src/train_explicit.py" \
        --infile ${IN_IMG_FPATH} \
        --inlbl ${IN_LBL_FPATH} \
        --experiment ${EXPERIMENT} \
        --weight_dir ${WEIGHT_DIR} \
        --n_aug_rots ${N_TRAIN_ROTS} \
        --gpu_id ${GPU_ID} \
        --n_cores ${N_CORES} \
        --n_items ${N_ITEMS} \
        --filters ${FILTERS} \
        --batch_size ${BATCH_SIZE} \
        --patch_size ${PATCH_SIZE} \
        --n_resblocks ${N_RESBLOCKS} \
        --kernel_size ${KERNEL_SIZE} \
        --acq_dim ${ACQ_DIM} \
        --noise_level ${NOISE_LEVEL} \
        --learning_rate ${LEARNING_RATE}
fi

if [ ${APPLY_TEST} = "true" ] ; then
    python "src/test_explicit.py" \
        --infile ${IN_IMG_FPATH} \
        --inlbl ${IN_LBL_FPATH} \
        --experiment ${EXPERIMENT} \
        --outfile ${OUT_IMG_FPATH} \
        --outlbl ${OUT_LBL_FPATH} \
        --weight_dir ${WEIGHT_DIR} \
        --gpu_id ${GPU_ID} \
        --n_cores ${N_CORES} \
        --filters ${FILTERS} \
        --patch_size ${PATCH_SIZE} \
        --n_resblocks ${N_RESBLOCKS} \
        --n_rots ${N_TEST_ROTS} \
        --kernel_size ${KERNEL_SIZE} \
        --acq_dim ${ACQ_DIM} \
        --img_p ${FBA_IMG_P} \
        --lbl_p ${FBA_LBL_P}
fi
