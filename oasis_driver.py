import os
import sys
import numpy as np
from pathlib import Path


CHUNK_SIZE = 10


def chunks(l, n):
    n = max(1, n)
    return [l[i:i+n] for i in range(0, len(l), n)]


gpuid = sys.argv[1]
partition = int(sys.argv[2])

IN_IMG_DIR = Path("/ISFILE3/USERS/remediossw/data/OASIS3/OASIS3_4x_aniso")
IN_LBL_DIR = Path(
    "/ISFILE3/USERS/remediossw/data/OASIS3/OASIS3_4x_aniso_toads_labels")

img_fpaths = sorted(IN_IMG_DIR.iterdir())
lbl_fpaths = sorted(IN_LBL_DIR.iterdir())

# Turn off partitions for a quick next check. Missed some for some reason.
# split into partitions
img_fpaths = chunks(img_fpaths, CHUNK_SIZE)[partition]
lbl_fpaths = chunks(lbl_fpaths, CHUNK_SIZE)[partition]

IMG_RESULT_PREFIX = "SR_img_"
LBL_RESULT_PREFIX = "SR_lbl_"

for a, b in zip(img_fpaths, lbl_fpaths):
    fname = a.name
    print("Processing {}".format(fname))

    call = "./apply_ssr.sh {} OASIS {} {}"
    os.system(call.format(gpuid, a, b))
