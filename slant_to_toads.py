import nibabel as nib
import numpy as np
import sys

from pathlib import Path
from tqdm import tqdm

mapping_file = Path(sys.argv[1])
IN_DIR = Path(sys.argv[2])
OUT_DIR = Path(sys.argv[3])

if not OUT_DIR.exists():
    OUT_DIR.mkdir(parents=True)


# Get mapping from file
with open(mapping_file, "r") as f:
    lines = f.readlines()
# discard header
lines = lines[1:]

remap = {}
for line in lines:
    k, v, _ = line.split()
    remap[int(k)] = int(v)


for fpath in tqdm(IN_DIR.iterdir()):
    mask_obj = nib.load(fpath)
    affine = mask_obj.affine
    header = mask_obj.header
    mask = mask_obj.get_fdata()

    mask_toads = mask.copy()
    for v in np.unique(mask):
        mask_toads[np.where(mask == v)] = remap[int(v)]

    mask_toads_obj = nib.Nifti1Image(mask_toads, affine=affine, header=header)

    out_fname = "toads" + fpath.name[5:]
    out_fpath = OUT_DIR / out_fname
    nib.save(mask_toads_obj, out_fpath)
